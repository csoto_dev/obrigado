<?php
require('libs/conexion.php');
require('libs/fpdf.php');
$id_prorroga=$_GET['id'];
$queryprorroga=mysqli_query($conexion, "SELECT a.id_acto, a.dirigidoa,a.fecha_emision, a.motivo, a.cantidad_dias, a.tipo_dias, a.cargoa, b.n_req, b.id_inst, c.nro, d.nombre AS instituto, d.dependencia FROM prorrogas a, actos b, odc_acto c, instituciones d WHERE id_prorroga='$id_prorroga' AND b.id_acto=a.id_acto AND c.id_acto=a.id_acto AND d.id_inst=b.id_inst");
$arrayprorroga=mysqli_fetch_array($queryprorroga);

$cantidad_dias=$arrayprorroga['cantidad_dias'];
$tipo_dias=$arrayprorroga['tipo_dias'];
$motivo=$arrayprorroga['motivo'];
$nombre=$arrayprorroga['dirigidoa'];
$cargo=$arrayprorroga['cargoa'];
$dependencia=$arrayprorroga['dependencia'];
$instututo=$arrayprorroga['instituto'];
$odc=$arrayprorroga['nro'];
$req=$arrayprorroga['n_req'];
$fecha_emision=$arrayprorroga['fecha_emision'];

$dia=date('d', strtotime($fecha_emision));
$mes_eng=date('F', strtotime($fecha_emision));
$ano=date('Y', strtotime($fecha_emision));
$meses_esp=array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
$meses_eng=array('January','February','March','April','May','June','July','August','September','October','November','December');
$mes_esp=str_replace($meses_eng, $meses_esp, $mes_eng);

class PDF extends FPDF
{
// Cabecera de p�gina
function Header()
{
	// Logo
	$this->Image('logo.png',10,8,50);
	// Arial bold 15
	$this->SetFont('Arial','I',12);
	// Movernos a la derecha
	$this->Ln(20);
}

// Pie de p�gina
function Footer()
{
	// Posici�n: a 1,5 cm del final
	$this->SetY(-25);
	// Arial italic 8
	$this->SetFont('Arial','I',8);
	// N�mero de p�gina
	$this->Cell(0,5,'DIRECCI�N: SAN FRANCISCO, CALLE ISAAC HANONO, OCEANIA BUSINESS PLAZA, TORRE 3000, PISO 6.',0,1,'C');

	$this->Cell(0,5,'TEL�FONO: (+507)387-6392 / (+507)387-6393. E-MAIL: INFO@OBRIGADOMEDICALGROUP.COM.',0,1,'C');
	$this->Cell(0,5,'WEBSITE: HTTPS://WWW.OBRIGADOMEDICALGROUP.COM',0,1,'C');
}
}

// Creaci�n del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',12);
$pdf->Cell(0,7,'Panam�, '.$dia.' de '.$mes_esp.' de '.$ano.'',0,1, 'R');

$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,7,''.$dependencia.'',0,1, 'L');
$pdf->Cell(0,7,''.$nombre.'',0,1, 'L');
$pdf->Cell(0,7,''.$cargo.'',0,1, 'L');
$pdf->Cell(0,7,''.$instututo.'',0,1, 'L');
$pdf->Cell(0,7,'PRESENTE.-',0,1, 'L');
$pdf->ln(10);
$pdf->SetFont('Arial','',12);
$pdf->Cell(0,7,'Ref. Orden de Compra No.:',0,1, 'R');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,7, ''.$odc.'/'.$req.'',0,0, 'R');
$pdf->ln(15);
$pdf->SetX(-200);
$pdf->SetFont('Arial','',12);
$pdf->MultiCell(190, 7, '     Por este medio solicitamos una pr�rroga de '.$cantidad_dias.' d�as '.$tipo_dias.' para efectuar la entrega de la siguiente mercanc�a indicada en la orden de compra en menci�n:', 0, 'J',false);
$pdf->ln(7);
$pdf->SetFont('Arial','B',12);
$queryproduct=mysqli_query($conexion, "SELECT a.id_propuesta, b.id_producto, c.descripcion, c.marca FROM productos_prorroga a, productos_participacion b, productos c WHERE a.id_prorroga='$id_prorroga' AND b.id_participacion=a.id_propuesta AND  c.id_producto=b.id_producto") or die (mysqli_error($conexion));
while ($arrayproducts=mysqli_fetch_array($queryproduct)) {	
	$pdf->Cell(0,7,''.$arrayproducts['descripcion'].', marca: '.$arrayproducts['marca'].'',0,1, 'C');
}
$pdf->SetFont('Arial','',12);
$pdf->ln(7);
$pdf->MultiCell(190, 7, '     Nuestra solicitud se basa en, que '.$motivo.'. La empresa realizara un gran esfuerzo por cumplir lo antes posible con la entrega.', 0, 'J',false);
$pdf->ln(7);
$pdf->MultiCell(190, 7, 'Ofrecemos disculpas por los inconvenientes que nuestra demora pudiese causar.', 0, 'J',false);
$pdf->MultiCell(190, 7, 'Agradecido por su atencion y sin mas a que hacer referencia. ', 0, 'J',false);
$pdf->Cell(0,7,'En nombre de Obrigado Medical Group S.A., se suscribe.', 0, 1, 'L');
$pdf->ln(30);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,7,'______________________________',0,1, 'C');
$pdf->Cell(0,7,'Ruben R Rose B',0,1, 'C');
$pdf->Cell(0,7,'Representante Legal',0,1, 'C');

$pdf->Output('', 'prorroga-acto.pdf','');
?>
