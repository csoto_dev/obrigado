<?php
	$namepage=array();
	include ("libs/conexion.php");
	
	session_start();
	if(!isset($_SESSION['usuario'])){
		echo "<script>alert('Error 03: No puede acceder sin iniciar sesion.'); window.location='index.html';</script>";
	}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>OMG-GCS</title>


<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/bootstrap-table.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">

<!--Icons-->
<script src="js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->
<style type="text/css">
	body{
		padding: 0;
	}
	.col-sm-9, .col-sm-offset-3, .col-lg-10, .col-lg-offset-2, .main{
		margin: 0;
		width: 100%;
	}
	.header-casita{
		display: none;
	}
</style>
</head>

<body>
		
	<?php 
		if(isset($_GET['p'])){
			$p=$_GET['p']; 
			include($p.'.html');
		}else{			
			include('inicio.html');
		}


	?>
		

	  

</body>

</html>
