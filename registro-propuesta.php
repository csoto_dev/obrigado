<?php
	include('libs/conexion.php');
	$id=$_GET['id'];
	if(isset($_POST['id_renglon'])){
		$id_renglon=$_POST['id_renglon'];
		$queryreng=mysqli_query($conexion, "SELECT * FROM renglones_acto WHERE id_renglon='$id_renglon'") or die (mysqli_error($conexion));
		$arrayreng=mysqli_fetch_array($queryreng);
		$cantidad=$arrayreng['cantidad'];
	}else{
		$id_renglon="''";
		$cantidad="''";
	}
	session_start();
	if(!isset($_SESSION['usuario'])){
		echo "<script>alert('Error 03: No puede acceder sin iniciar sesion.'); window.location='index.html';</script>";
	}

	include('libs/renglones.php');


	if(isset($_POST['id_producto'])){
		$id_producto=$_POST['id_producto'];
		$queryproduct=mysqli_query($conexion, "SELECT * FROM productos WHERE id_producto='$id_producto'") or die (mysqli_error($conexion));
		$arrayproduct=mysqli_fetch_array($queryproduct);
		$codigo=$arrayproduct['codigo'];
		$descripcion=$arrayproduct['descripcion'];
		$marca=$arrayproduct['marca'];
		$unidad_medida=$arrayproduct['unidad_medida'];
	}else{
		$id_producto='';
		$codigo='';
		$descripcion='';
		$marca='';
		$unidad_medida='';

	}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>.:Obrigado Licitaciones:.</title>


<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/bootstrap-table.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">

<!--Icons-->
<script src="js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body style="padding: 0;">
			<div class="row" style="margin:0;">
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">Renglones del acto</div>
						<div class="panel-body">
							<form name="form" id='formulario' method="POST">
							<table id='listado-renglones' data-select-item-name="id_acto" >
							    <thead>
							    <tr>
							        <th data-field="id" data-checkbox="true" ># Item</th>
							        <th data-field="reng" data-align="right" data-sortable="true">#Reng.</th>
							        <th data-field="codigo" data-sortable="true">C&oacute;digo</th>
							        <th data-field="descripcion"  data-sortable="true">Descripci&oacute;n</th>
							        <th data-field="clasificacion"  data-sortable="true">Clasificaci&oacute;n</th>
							        <th data-field="um"  data-sortable="true">Unidad de medida</th>
							        <th data-field="cantidad"  data-sortable="true">Cantidad</th>
							    </tr>
							    </thead>
							</table>

							</form>
						</div>
					</div>
				</div>
			</div>

			<div class="row" style="margin:0;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">Registro de propuestas</div>
						<div class="panel-body">
							<form role="form" name="formbusqueda" action="busqueda.php" method="POST">
								<div class="col-md-6">								
									<div class="form-group" id="vbusqueda">
										<label style="display:block;">Buscar producto:</label>
										<input class="form-control" name="busqueda" id="busqueda" placeholder="Buscar producto" style="width:70%; display:inline-block;">
										<input type='hidden' id='id_renglon' name="id_renglon" value=''>
										<input type='hidden' name="id_acto" value='<?php echo $id;?>'>
										<input type='hidden' name="url" value='<?php echo $_SERVER["REQUEST_URI"];?>'>
										<button type="button" onclick="buscar()" class="btn btn-primary">Buscar</button>
									</div>
									
								</div>
							</form>
							<form role="form" name="formulario" action="" method="POST">
							<div class="col-md-6">
									<div class="form-group" id="vcode">
										<label>C&oacute;digo del producto</label>
										<input class="form-control" id="code" placeholder="Unidad de medida" value='<?php echo $codigo;?>' readonly>
										<input type='hidden' id='id_renglon2' name="id_renglon" value=''>
									</div>
									<div class="form-group" id="vdesc">
										<label>Descripci&oacute;n del producto</label>
										<textarea class="form-control" id="" placeholder="Descrici&oacute;n del producto" readonly><?php echo $descripcion;?></textarea>
									</div>
									<div class="form-group" id="vmarca">
										<label>Marca del producto</label>
										<input class="form-control" id="" placeholder="Marca del producto" value='<?php echo $marca;?>' readonly>
									</div>				
									
									<div class="form-group" id="vum">
										<label>Unidad de medida</label>
										<input class="form-control" name="unid_medida" id="um" placeholder="Unidad de medida" value='<?php echo $unidad_medida;?>' readonly>
									</div>
									<div class="form-group" id="vitbm">
										<label>&iquest;ITBMS?</label>
										<div class="radio">
											<label>
												<input name="itbm" id="itbm1" value="1" checked="" type="Radio">Si
											</label>
										</div>
										<div class="radio">
											<label>
												<input name="itbm" id="itbm2" value="0" type="Radio">No
											</label>
										</div>
									</div>
									<div class="form-group" id="vpreciou">
										<label>Precio unit.</label>
										<input class="form-control" name="preciou" type="number" value='0' step="0.01" min="0" id="preciou" placeholder="Clasificaci&oacute;n" onchange="cambiar_total()">
										<input type='hidden' name="id_producto" value='<?php echo $id_producto;?>'>
									</div>
									<div class="form-group" id="vpreciou">
										<label>Cantidad</label>
										<input class="form-control" value="<?php echo $cantidad; ?>" readonly>
									</div>
									<div class="form-group" id="vsubtotal">
										<label>Sub-total</label>
										<input class="form-control" name="subtotal" id="subtotal" readonly="">
									</div>
									<div class="form-group" id="vitbms">
										<label>ITBMS</label>
										<input class="form-control" value='0' name="itbms" id="itbms" readonly="">
									</div>
									<div class="form-group" id="vpreciot">
										<label>Precio total.</label>
										<input class="form-control" name="preciot" id="preciot" readonly="">
									</div>
									
									
								</div>
									<div class="form-group" >	
										<button type="button" onclick="validar()" class="btn btn-primary">Registrar</button>
									</div>
							</form>
						</div>
					</div>
				</div><!-- /.col-->
			</div><!-- /.row -->
	  

	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/common.js"></script>
	<script src="js/datepicker.js"></script>
	<script src="js/datepicker.es-ES.js"></script>
	<script src="js/main.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/bootstrap-table.js"></script>		   
	<script type="text/javascript">
		
		var listado_renglones= <?php echo $listado_renglones;?>;
		$(function () {
		    $('#listado-renglones').bootstrapTable({
		        data: listado_renglones
		    });
		});
	</script>	
	<script>
		$(document).ready( function(){
  			$("input[type=checkbox]").each(function(){
				var valor = $(this).val();
				var seleccionado = <?php echo $id_renglon; ?>;
				if(valor == seleccionado){
					$(this).prop("checked", true);
				}
		    	$('#id_renglon2').val(seleccionado);
		    	$('.slcall').prop("checked", false);
		    	$('.slcall').prop("disabled", true);
		    	$('.slcall').prop("style", 'display:none');
			}); 
		});

		$(document).on('change', "input[type='checkbox']" , function(){
  
		  // in the handler, 'this' refers to the box clicked on
 		var $box = $(this);
		 if ($box.is(":checked")) {
		    // the name of the box is retrieved using the .attr() method
		    // as it is assumed and expected to be immutable
		    var group = "input:checkbox[name='" + $box.attr("name") + "']";
		    // the checked state of the group/box on the other hand will change
		    // and the current value is retrieved using .prop() method
		    $(group).prop("checked", false);
		    $box.prop("checked", true);
		    $('.slcall').prop("checked", false);
		    $('.slcall').prop("disabled", true);
		    $('#id_renglon').val($box.val());

		  } else {
		    $box.prop("checked", false);
		    $('#id_renglon').val('');
		  }
		});
	</script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
	<script>

		function cambiar_total() {
			var cantidad = <?php echo $cantidad; ?>;
			var preciou=document.getElementById('preciou').value; 			
			var subtotal = parseFloat(preciou).toFixed(2) * parseFloat(cantidad).toFixed(2);

			if($("input[type='radio']:checked").val() == 1){
				var itmbs = parseFloat(subtotal).toFixed(2) * 0.07;

			}else{
				var itmbs = 0;

			}
			

			var preciotot = subtotal + itmbs;

			document.getElementById('subtotal').value=subtotal;
			document.getElementById('itbms').value=itmbs.toFixed(2);

			document.getElementById('preciot').value=parseFloat(preciotot).toFixed(2);


			// body...
		}
		function validar() {
			var codigo=document.getElementById('code').value;			
			var um=document.getElementById('um').value;	
			var preciou=document.getElementById('preciou').value;
			var preciot=document.getElementById('preciot').value;			
			var error=0;		

			document.getElementById("vcode").className -= " has-error";
			document.getElementById("vdesc").className -= " has-error";
			document.getElementById("vmarca").className -= " has-error";
			document.getElementById("vum").className -= " has-error";
			document.getElementById("vpreciou").className -= " has-error";
			document.getElementById("vpreciot").className -= " has-error";


			if(codigo.length==0){
				alert('Debe seleccionar un producto.');
				document.getElementById("vcode").className += " has-error";
				document.getElementById("vdesc").className += " has-error";
				document.getElementById("vmarca").className += " has-error";
				error++;
				
			}

			if(um.length==0){
				alert('El campo "Unidad de medida" no puede ir vacio.');
				document.getElementById("vum").className += " has-error";
				error++;
				
			}

			if(preciou.length==0){
				alert('El campo "Unidad de medida" no puede ir vacio.');
				document.getElementById("vpreciou").className += " has-error";
				error++;
				
			}

			if(preciot.length==0){
				alert('El campo "Unidad de medida" no puede ir vacio.');
				document.getElementById("vpreciot").className += " has-error";
				error++;
				
			}

			if(error==0){
				document.formulario.action= "reg_propuesta.php";
				document.formulario.submit();
			}
		}

	    // Controlamos que si pulsamos escape se cierre el div

	    $(document).keyup(function(event){
	        if(event.which==27){
				window.close();
	        }
	    });

	    function buscar() {
	    	var id_renglon=document.getElementById('id_renglon').value;
			var error=0;
			if(id_renglon.length==0){
				alert('Debe primero seleccionar un renglon.');
				error++;
				
			}
			if(error==0){
				document.formbusqueda.action= "busqueda.php";
				document.formbusqueda.submit();
			}
	    }
	</script>	
	
</body>

</html>
