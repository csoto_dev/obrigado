-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 25, 2018 at 07:32 PM
-- Server version: 5.7.17-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `obrigado_gcs`
--

-- --------------------------------------------------------

--
-- Table structure for table `actas_adjudicacion`
--

CREATE TABLE `actas_adjudicacion` (
  `id_acta` int(11) NOT NULL,
  `fecha_emision` date NOT NULL,
  `id_acto` int(11) NOT NULL,
  `ubicacion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `actas_adjudicacion`
--

INSERT INTO `actas_adjudicacion` (`id_acta`, `fecha_emision`, `id_acto`, `ubicacion`) VALUES
(16, '2018-05-04', 32, 'documentos_actos/actas_adjudicacion/acta_adjudicacion_32.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `actas_decision`
--

CREATE TABLE `actas_decision` (
  `id_decision` int(11) NOT NULL,
  `id_acto` int(11) NOT NULL,
  `fecha_emision` date NOT NULL,
  `ubicacion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `actas_decision`
--

INSERT INTO `actas_decision` (`id_decision`, `id_acto`, `fecha_emision`, `ubicacion`) VALUES
(9, 23, '2018-04-30', 'documentos_actos/actas_decision/acta_desicion_23.pdf'),
(10, 24, '2018-05-10', 'documentos_actos/actas_decision/acta_desicion_24.pdf'),
(11, 26, '2018-06-01', 'documentos_actos/actas_decision/acta_desicion_26.pdf'),
(12, 28, '2018-05-30', 'documentos_actos/actas_decision/acta_desicion_28.pdf'),
(13, 29, '2018-05-21', 'documentos_actos/actas_decision/acta_desicion_29.pdf'),
(14, 30, '2018-05-21', 'documentos_actos/actas_decision/acta_desicion_30.pdf'),
(15, 31, '2018-05-08', 'documentos_actos/actas_decision/acta_desicion_31.pdf'),
(16, 32, '2018-05-04', 'documentos_actos/actas_decision/acta_desicion_32.pdf'),
(19, 36, '2018-06-05', 'documentos_actos/actas_decision/acta_desicion_36.pdf'),
(18, 34, '2018-05-04', 'documentos_actos/actas_decision/acta_desicion_34.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `actos`
--

CREATE TABLE `actos` (
  `id_acto` int(11) NOT NULL,
  `n_acto` varchar(100) NOT NULL,
  `n_req` varchar(50) NOT NULL,
  `id_inst` int(11) NOT NULL,
  `fecha_publicacion` date NOT NULL,
  `fecha_acto` date NOT NULL,
  `monto_act` decimal(50,2) NOT NULL,
  `tipo_acto` int(11) NOT NULL,
  `termino_pago` text NOT NULL,
  `termino_pago_dias` int(11) NOT NULL,
  `termino_pago_tipo` varchar(15) NOT NULL,
  `fdp` int(11) NOT NULL,
  `fdc` int(11) NOT NULL,
  `fde` int(11) NOT NULL,
  `ganador` text NOT NULL,
  `calificacion` int(11) NOT NULL,
  `fecha_max_fdc` date NOT NULL,
  `fecha_max_entrega` date NOT NULL,
  `nro_prorrogas` int(11) NOT NULL,
  `id_status` int(11) NOT NULL,
  `vendedor` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `actos`
--

INSERT INTO `actos` (`id_acto`, `n_acto`, `n_req`, `id_inst`, `fecha_publicacion`, `fecha_acto`, `monto_act`, `tipo_acto`, `termino_pago`, `termino_pago_dias`, `termino_pago_tipo`, `fdp`, `fdc`, `fde`, `ganador`, `calificacion`, `fecha_max_fdc`, `fecha_max_entrega`, `nro_prorrogas`, `id_status`, `vendedor`) VALUES
(37, '2018-0-36-0-08-CM-010227', '2018-010227', 23, '2018-05-08', '2018-05-14', '11998.98', 1, '', 90, 'Calendario', 0, 0, 0, '', 0, '0000-00-00', '0000-00-00', 0, 1, '4'),
(38, '2018-1-10-0-08-LP-300322', '1000501907', 15, '2018-06-05', '2018-06-12', '48940.00', 1, '', 90, 'Calendario', 1, 1, 0, '', 0, '0000-00-00', '0000-00-00', 0, 1, '4'),
(32, '2018-1-10-0-08-LP-294705', '1000497570', 15, '2018-04-26', '2018-05-04', '13500.00', 1, '', 90, 'Calendario', 1, 1, 0, 'Obrigado', 1, '2018-05-11', '2018-08-27', 0, 6, '4'),
(31, '2018-0-36-0-08-CM-010142', '146-2018', 23, '2018-04-09', '2018-04-13', '11663.00', 1, '', 1, 'Calendario', 0, 0, 0, 'MANUEL JARAMILLO CASTILLO', 5, '0000-00-00', '0000-00-00', 0, 12, '4'),
(30, '2018-0-36-0-08-CM-010209', '158-2018', 23, '2018-04-25', '2018-05-03', '14552.00', 1, '', 1, 'Calendario', 0, 0, 0, 'COMPAÑIA DISTRIBUIDORA JEMA S.A', 2, '0000-00-00', '0000-00-00', 0, 12, '4'),
(29, '2018-0-12-222-15-CM-004419', '18-23578', 22, '2018-04-19', '2018-04-27', '49755.00', 1, '', 1, 'Habiles', 0, 0, 0, 'alpha mediq s.a', 5, '0000-00-00', '0000-00-00', 0, 12, '4'),
(28, '2018-1-90-0-08-CM-042708', 'CHVC-2018-37-02', 18, '2018-04-13', '2018-04-19', '17500.00', 1, '', 45, 'Habiles', 0, 0, 0, 'MIRERO CORP', 5, '0000-00-00', '0000-00-00', 0, 12, '4'),
(27, '2018-0-04-0-08-CM-010229', '2018-00294', 21, '2018-04-16', '2018-04-23', '40628.97', 2, '', 1, 'Habiles', 0, 0, 0, '', 0, '0000-00-00', '0000-00-00', 0, 2, '4'),
(24, '2018-1-11-0-08-CM-008032', '250-2018', 19, '2018-04-10', '2018-04-18', '14500.00', 1, '', 30, 'Calendario', 0, 0, 0, 'COMPAÑIA DISTRIBUIDORA JEMA S.A', 5, '0000-00-00', '0000-00-00', 0, 12, '4'),
(26, '2018-1-10-0-02-LP-293468', '1000501345', 20, '2018-04-16', '2018-04-20', '9000.00', 1, '', 1, 'Habiles', 0, 0, 0, 'Importadora CBC S.A', 5, '0000-00-00', '0000-00-00', 0, 12, '4'),
(23, '2018-0-12-22-08-CM-001746', '118-18', 17, '2018-04-16', '2018-04-23', '43000.00', 1, '', 30, 'Habiles', 0, 0, 0, 'infinity medical', 2, '0000-00-00', '0000-00-00', 0, 12, '4'),
(34, '2018-1-10-0-08-LP-294707', '1000504568', 15, '2018-04-26', '2018-05-04', '12900.00', 1, '', 90, 'Calendario', 1, 1, 0, 'HORACIO ICAZA', 2, '0000-00-00', '0000-00-00', 0, 12, '4'),
(35, '2018-1-10-0-08-LP-294701', '1000497829', 15, '2018-04-26', '2018-05-04', '16800.00', 1, '', 90, 'Calendario', 1, 1, 0, '', 0, '0000-00-00', '0000-00-00', 0, 2, '4'),
(36, '2018-0-36-0-08-CM-010252', '2018-010252', 23, '2018-05-04', '2018-05-10', '25325.00', 1, '', 90, 'Calendario', 0, 0, 0, 'COMPAÑIA Y DISTRIBUIDORA JEMA', 5, '0000-00-00', '0000-00-00', 0, 12, '4');

-- --------------------------------------------------------

--
-- Table structure for table `calificaciones`
--

CREATE TABLE `calificaciones` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `calificaciones`
--

INSERT INTO `calificaciones` (`id`, `nombre`) VALUES
(0, 'Pendiente por evaluacion'),
(1, 'Mejor Oferta'),
(2, 'Segundo Lugar'),
(3, 'Tercer Lugar'),
(4, 'Cuarto Lugar'),
(5, 'Descalificado'),
(6, 'Perdido por precio');

-- --------------------------------------------------------

--
-- Table structure for table `compras`
--

CREATE TABLE `compras` (
  `id_compra` int(11) NOT NULL,
  `id_acto` text NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `fecha_compra` date NOT NULL,
  `metodo_pago` varchar(50) NOT NULL,
  `costo_fob` decimal(50,2) NOT NULL,
  `utilidad` decimal(3,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `detalle_compra`
--

CREATE TABLE `detalle_compra` (
  `id_detalle_c` int(11) NOT NULL,
  `id_compra` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `nro_item` varchar(100) NOT NULL,
  `nro_track` varchar(100) NOT NULL,
  `cantidad_caja` int(11) NOT NULL,
  `cantidad_piezas` int(11) NOT NULL,
  `cantidad_arribada` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `detalle_entrega`
--

CREATE TABLE `detalle_entrega` (
  `id_detalle` int(11) NOT NULL,
  `id_entrega` int(11) NOT NULL,
  `id_renglon` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `detalle_ventas_ip`
--

CREATE TABLE `detalle_ventas_ip` (
  `id_detalle` int(11) NOT NULL,
  `id_venta` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_unit` decimal(50,2) NOT NULL,
  `precio_tot` decimal(50,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `documentos_prorrogas`
--

CREATE TABLE `documentos_prorrogas` (
  `id_documento` int(11) NOT NULL,
  `id_prorroga` int(11) NOT NULL,
  `tipo` text NOT NULL,
  `ubicacion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `entregas`
--

CREATE TABLE `entregas` (
  `id_entrega` int(11) NOT NULL,
  `id_acto` int(11) NOT NULL,
  `tipo_entrega` text NOT NULL,
  `fecha_entrega` date NOT NULL,
  `direccion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `estado_licitaciones`
--

CREATE TABLE `estado_licitaciones` (
  `id_status` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `estado_licitaciones`
--

INSERT INTO `estado_licitaciones` (`id_status`, `nombre`) VALUES
(1, 'Pendiente por participar'),
(2, 'Pendiente acta de decision'),
(3, 'Pendiente acta de adjudicacion'),
(4, 'Pendiente fianza de cumplimiento'),
(5, 'Pendiente orden de compra'),
(6, 'Pendiente por entregar'),
(7, 'Entregado/Pendiente informe Recep.'),
(8, 'Pendiente pago de multa'),
(9, 'Pendiente presentacion de cta.'),
(10, 'Pendiente pago'),
(11, 'Cobrada'),
(12, 'Perdida'),
(13, 'Anulada');

-- --------------------------------------------------------

--
-- Table structure for table `estado_prorogas`
--

CREATE TABLE `estado_prorogas` (
  `id_estado` int(11) NOT NULL,
  `nombre` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `estado_prorogas`
--

INSERT INTO `estado_prorogas` (`id_estado`, `nombre`) VALUES
(1, 'Emitida'),
(2, 'Entregada/Por respuesta'),
(3, 'Respondida'),
(4, 'Anulada');

-- --------------------------------------------------------

--
-- Table structure for table `estado_usuarios`
--

CREATE TABLE `estado_usuarios` (
  `id_estado` int(11) NOT NULL,
  `nombre` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `estado_usuarios`
--

INSERT INTO `estado_usuarios` (`id_estado`, `nombre`) VALUES
(0, 'Inactivo'),
(1, 'Activo');

-- --------------------------------------------------------

--
-- Table structure for table `facturas_actos`
--

CREATE TABLE `facturas_actos` (
  `id_factura` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `monto` decimal(50,2) NOT NULL,
  `ubicacion` text NOT NULL,
  `id_acto` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fdc`
--

CREATE TABLE `fdc` (
  `id` int(11) NOT NULL,
  `fecha_emision` date NOT NULL,
  `fecha_entrega` date NOT NULL,
  `ubicacion` text NOT NULL,
  `id_acto` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fdc`
--

INSERT INTO `fdc` (`id`, `fecha_emision`, `fecha_entrega`, `ubicacion`, `id_acto`) VALUES
(18, '2018-05-15', '2018-05-17', 'documentos_actos/FDC/FDC-32.pdf', 32);

-- --------------------------------------------------------

--
-- Table structure for table `fde`
--

CREATE TABLE `fde` (
  `id_fde` int(11) NOT NULL,
  `id_acto` int(11) NOT NULL,
  `fecha_solicitud` date NOT NULL,
  `ubicacion_solicitud` text NOT NULL,
  `fecha_emision` date NOT NULL,
  `ubicacion_emision` text NOT NULL,
  `fecha_entrega` date NOT NULL,
  `ubicacion_recibido` text NOT NULL,
  `estado` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `feriados`
--

CREATE TABLE `feriados` (
  `id` int(11) NOT NULL,
  `dia` date NOT NULL,
  `descripcion` text NOT NULL,
  `repite` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `informes_recepcion`
--

CREATE TABLE `informes_recepcion` (
  `id_informe` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `id_acto` int(11) NOT NULL,
  `ubicacion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `instituciones`
--

CREATE TABLE `instituciones` (
  `id_inst` int(11) NOT NULL,
  `tipo` text NOT NULL,
  `dependencia` text NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `direccion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `instituciones`
--

INSERT INTO `instituciones` (`id_inst`, `tipo`, `dependencia`, `telefono`, `nombre`, `direccion`) VALUES
(19, 'PUBLICO', 'AUTONOMO', '527-4860', 'INSTITUTO CONMEMORATIVO GORGAS DE ESTUDIOS DE LA SALUD', 'AVENIDA JUSTO AROSEMENA'),
(7, 'PUBLICO', 'MINISTERIO DE SALUD', '507-5824', 'PATRONATO HOSPITAL SANTO TOMAS', 'AVENIDA JUSTO AROSEMENA'),
(17, 'PUBLICO', 'MINISTERIO DE SALUD', '512-6800', 'INSTITUTO NACIONAL DE MEDICINA FISICA Y REHABILITACION', 'Via centenario, Panama '),
(16, 'PRIVADO', '- - -', '64891812', 'PACIFICA SALUD', 'CALLE ISAAC HANONO'),
(15, 'PUBLICO', 'CAJA DEL SEGURO SOCIAL', '503-6600', 'COMPLEJO HOSPITALARIO DR ARNULFO ARIAS MADRID', 'Via Simon Bolivar, Panama City'),
(18, 'PUBLICO', 'AUTONOMO', '523-5159', 'UNIVERSIDAD DE PANAMA', 'TRANSISMICA, PANAMA'),
(20, 'PUBLICO', 'CAJA DEL SEGURO SOCIAL', '9979209', 'POLICLINICA MANUEL PAULINO ACAÑA', 'PROVINCIA DE COCLE'),
(21, 'PUBLICO', 'AUTONOMO ', '512-2060', 'MINISTERIO DE GOBIERNO ', 'Calle Tercera San Felipe, a un costado del Teatro Nacional'),
(22, 'PUBLICO', 'MINISTERIO DE SALUD', '254-8950', 'HOSPITAL NICOLAS A. SOLANO ', 'CHORRERA'),
(23, 'PUBLICO', 'AUTONOMO', '507-2242', 'INSTITUTO DE MEDICINA LEGAL Y CIENCIAS FORENSES', 'RESIDENCIAL LLANOS DE CURUNDU'),
(24, 'PUBLICO', 'MINISTERIO DE SALUD', '775-4222', 'HOSPITAL JOSE DOMINGO DE OBALDIA', 'Carr Panamericana, San Pablo Viejo 507');

-- --------------------------------------------------------

--
-- Table structure for table `licitaciones`
--

CREATE TABLE `licitaciones` (
  `id_licitacion` int(11) NOT NULL,
  `id_actos` int(11) NOT NULL,
  `id_participante` int(11) NOT NULL,
  `fianza_p` varchar(100) NOT NULL,
  `fianza_c` varchar(100) NOT NULL,
  `monto_propuesta` varchar(100) NOT NULL,
  `monto_fob` varchar(100) NOT NULL,
  `nacionalizacion` varchar(100) NOT NULL,
  `envio` varchar(100) NOT NULL,
  `itbm` varchar(100) NOT NULL,
  `comision_compra` varchar(100) NOT NULL,
  `comision_venta` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `licitaciones`
--

INSERT INTO `licitaciones` (`id_licitacion`, `id_actos`, `id_participante`, `fianza_p`, `fianza_c`, `monto_propuesta`, `monto_fob`, `nacionalizacion`, `envio`, `itbm`, `comision_compra`, `comision_venta`) VALUES
(5, 1, 1, '', '', '1414', '123412341234', '2131321', '213412341234', '21341234', '123412341234', '1234123412');

-- --------------------------------------------------------

--
-- Table structure for table `multas`
--

CREATE TABLE `multas` (
  `id_multa` int(11) NOT NULL,
  `id_acto` int(11) NOT NULL,
  `dias` int(11) NOT NULL,
  `porcentaje` int(11) NOT NULL,
  `ubicacion` text NOT NULL,
  `fecha` date NOT NULL,
  `monto` decimal(50,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `multas`
--

INSERT INTO `multas` (`id_multa`, `id_acto`, `dias`, `porcentaje`, `ubicacion`, `fecha`, `monto`) VALUES
(23, 20, 12, 1, 'documentos_actos/multas/multa-20.pdf', '2017-12-20', '123.00'),
(25, 21, 15, 3, 'documentos_actos/multas/multa-21.pdf', '2018-06-04', '150000.00'),
(26, 22, 12, 3, 'documentos_actos/multas/multa-22.pdf', '2019-08-31', '150.00');

-- --------------------------------------------------------

--
-- Table structure for table `odc_acto`
--

CREATE TABLE `odc_acto` (
  `id_odc` int(11) NOT NULL,
  `id_acto` int(11) NOT NULL,
  `nro` text NOT NULL,
  `fecha` date NOT NULL,
  `fecha_vto` date NOT NULL,
  `monto` decimal(50,2) NOT NULL,
  `ubicacion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `odc_acto`
--

INSERT INTO `odc_acto` (`id_odc`, `id_acto`, `nro`, `fecha`, `fecha_vto`, `monto`, `ubicacion`) VALUES
(6, 32, '3000206977', '2018-05-18', '2018-11-25', '13000.00', 'documentos_actos/ODC/ODC-32.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `pagos`
--

CREATE TABLE `pagos` (
  `id_pago` int(11) NOT NULL,
  `id_acto` int(11) NOT NULL,
  `ref` text NOT NULL,
  `tipo` text NOT NULL,
  `fecha` date NOT NULL,
  `ubicacion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `participantes`
--

CREATE TABLE `participantes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `direccion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `presentacion_cuenta`
--

CREATE TABLE `presentacion_cuenta` (
  `id_pdc` int(11) NOT NULL,
  `id_acto` int(11) NOT NULL,
  `fechade_pdc` date NOT NULL,
  `ubicacion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `privilegios_usuarios`
--

CREATE TABLE `privilegios_usuarios` (
  `id_usuario` int(11) NOT NULL,
  `licitaciones` int(11) NOT NULL,
  `registrar_licitacion` int(11) NOT NULL,
  `consultar_licitacion` int(11) NOT NULL,
  `actualizar_licitacion` int(11) NOT NULL,
  `anular_licitacion` int(11) NOT NULL,
  `entregas` int(11) NOT NULL,
  `registrar_entrega` int(11) NOT NULL,
  `anular_entrega` int(11) NOT NULL,
  `consultar_entrega` int(11) NOT NULL,
  `prorrogas` int(11) NOT NULL,
  `registrar_prorroga` int(11) NOT NULL,
  `actualizar_prorroga` int(11) NOT NULL,
  `anular_prorroga` int(11) NOT NULL,
  `consultar_prorroga` int(11) NOT NULL,
  `clientes` int(11) NOT NULL,
  `registrar_cliente` int(11) NOT NULL,
  `actualizar_cliente` int(11) NOT NULL,
  `consultar_cliente` int(11) NOT NULL,
  `fde` int(11) NOT NULL,
  `registrar_fde` int(11) NOT NULL,
  `consultar_fde` int(11) NOT NULL,
  `actualizar_fde` int(11) NOT NULL,
  `anular_fde` int(11) NOT NULL,
  `ventas_ip` int(11) NOT NULL,
  `registrar_ventas_ip` int(11) NOT NULL,
  `consultar_ventas_ip` int(11) NOT NULL,
  `entregar_ventas_ip` int(11) NOT NULL,
  `anular_ventas_ip` int(11) NOT NULL,
  `usuarios` int(11) NOT NULL,
  `registrar_usuario` int(11) NOT NULL,
  `actualizar_usuario` int(11) NOT NULL,
  `activar_usuario` int(11) NOT NULL,
  `desactivar_usuario` int(11) NOT NULL,
  `reportes` int(11) NOT NULL,
  `reportes_ventas` int(11) NOT NULL,
  `reportes_ventas_x_vendedor` int(11) NOT NULL,
  `reportes_gastos` int(11) NOT NULL,
  `reporte_compras` int(11) NOT NULL,
  `reporte_ingresos` int(11) NOT NULL,
  `reporte_analitico_gastos_ingresos` int(11) NOT NULL,
  `comunicaciones` int(11) NOT NULL,
  `registrar_comunicaciones` int(11) NOT NULL,
  `consultar_comunicaciones` int(11) NOT NULL,
  `compras` int(11) NOT NULL,
  `registrar_compra` int(11) NOT NULL,
  `actualizar_compra` int(11) NOT NULL,
  `consultar_compra` int(11) NOT NULL,
  `aunlar_compra` int(11) NOT NULL,
  `gastos` int(11) NOT NULL,
  `registrar_gasto` int(11) NOT NULL,
  `consultar_gasto` int(11) NOT NULL,
  `actualizar_gasto` int(11) NOT NULL,
  `anular_gasto` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `privilegios_usuarios`
--

INSERT INTO `privilegios_usuarios` (`id_usuario`, `licitaciones`, `registrar_licitacion`, `consultar_licitacion`, `actualizar_licitacion`, `anular_licitacion`, `entregas`, `registrar_entrega`, `anular_entrega`, `consultar_entrega`, `prorrogas`, `registrar_prorroga`, `actualizar_prorroga`, `anular_prorroga`, `consultar_prorroga`, `clientes`, `registrar_cliente`, `actualizar_cliente`, `consultar_cliente`, `fde`, `registrar_fde`, `consultar_fde`, `actualizar_fde`, `anular_fde`, `ventas_ip`, `registrar_ventas_ip`, `consultar_ventas_ip`, `entregar_ventas_ip`, `anular_ventas_ip`, `usuarios`, `registrar_usuario`, `actualizar_usuario`, `activar_usuario`, `desactivar_usuario`, `reportes`, `reportes_ventas`, `reportes_ventas_x_vendedor`, `reportes_gastos`, `reporte_compras`, `reporte_ingresos`, `reporte_analitico_gastos_ingresos`, `comunicaciones`, `registrar_comunicaciones`, `consultar_comunicaciones`, `compras`, `registrar_compra`, `actualizar_compra`, `consultar_compra`, `aunlar_compra`, `gastos`, `registrar_gasto`, `consultar_gasto`, `actualizar_gasto`, `anular_gasto`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE `productos` (
  `id_producto` int(11) NOT NULL,
  `codigo` text NOT NULL,
  `descripcion` text NOT NULL,
  `unidad_medida` text NOT NULL,
  `marca` text NOT NULL,
  `tipo` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productos`
--

INSERT INTO `productos` (`id_producto`, `codigo`, `descripcion`, `unidad_medida`, `marca`, `tipo`) VALUES
(3, 'FITMATE-WM', 'EQUIPO PARA LA MEDICION DEL GASTO DE ENERGIA DURANTE LA MARCHA', 'UNIDAD', 'COSMED', 2),
(4, 'YV-17369-20', 'CENTRIFUGA DE 24 POSICIONES PARA BOTELLAS BABCOCK', 'UNIDAD', 'COLE-PARMER', 2),
(5, 'GX.4.4-PRO', 'BICICLETA ESTACIONARIA SIN RESPALDAR', 'UNIDAD', 'NORDICTRACK', 2),
(8, 'YFQ029', 'ATRIL DE METAL DE DOS GANCHOS Y DOS RUEDAS', 'UNIDAD', 'YONGFA', 1),
(13, 'BC-5000VET', 'EQUIPO DE HEMATOLOGIA VETERINARIO MINDRAY ', 'UNIDAD', 'MINDRAY', 2),
(12, 'YFC-002', 'MESA PARA COMER PARA PACIENTES ', 'UNIDAD', 'YONGFA', 1),
(14, 'YFS-022', 'CARRO DE MEDICACION PARA UNIDOSIS ', 'UNIDAD', 'YONGFA', 1),
(15, 'OIML-F1', 'SET DE MASAS PARA CALIBRAR BALANZAS CLASE F1', 'UNIDAD', 'METLER TOLEDO ', 6),
(16, 'WD-T100', 'PURIFICADOR DE AGUA TIPO 1', 'UNIDAD', 'BIOBASE', 2),
(17, 'POLIPROPILENO', 'HOJA DE POLIPROPILENO', 'UNIDAD', 'QUADRANT', 7),
(19, 'NEOSHADES', 'PROTECTOR OCULAR PARA FOTOTERAPIA', 'UNIDAD', 'NEOTECH', 7),
(20, 'YFT-001', 'BALANZA DIGITAL ANALITICA DE ALTA RESOLUCION CON GRADILLA COLGANTE', 'UNIDAD', 'BIOBASE', 6),
(21, 'MDFU-33V-PA', 'CONGELADOR DE LABORATORIO TIPO GABINETE', 'UNIDAD', 'PANASONIC', 2);

-- --------------------------------------------------------

--
-- Table structure for table `productos_participacion`
--

CREATE TABLE `productos_participacion` (
  `id_participacion` int(11) NOT NULL,
  `id_renglon` int(11) NOT NULL,
  `id_acto` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `precio_unit` decimal(50,2) NOT NULL,
  `monto_total` decimal(50,2) NOT NULL,
  `itbms` decimal(50,2) NOT NULL,
  `subtotal` decimal(50,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productos_participacion`
--

INSERT INTO `productos_participacion` (`id_participacion`, `id_renglon`, `id_acto`, `id_producto`, `precio_unit`, `monto_total`, `itbms`, `subtotal`) VALUES
(24, 21, 31, 16, '9290.70', '9941.05', '0.00', '0.00'),
(16, 13, 23, 3, '38500.00', '38500.00', '0.00', '0.00'),
(17, 14, 24, 4, '11054.25', '11054.25', '0.00', '0.00'),
(18, 15, 26, 5, '3500.00', '3500.00', '0.00', '0.00'),
(19, 16, 27, 8, '134.00', '2680.00', '0.00', '0.00'),
(20, 17, 27, 12, '262.00', '5240.00', '0.00', '0.00'),
(21, 18, 28, 13, '12250.00', '12250.00', '0.00', '0.00'),
(22, 19, 29, 14, '1345.80', '14400.06', '0.00', '0.00'),
(23, 20, 30, 15, '14350.84', '14350.84', '0.00', '0.00'),
(25, 22, 32, 17, '130.00', '13000.00', '0.00', '13000.00'),
(27, 24, 34, 19, '3.75', '3750.00', '0.00', '3750.00'),
(28, 25, 35, 20, '14280.00', '14280.00', '0.00', '14280.00'),
(29, 26, 36, 21, '10484.50', '22436.83', '1467.83', '20969.00');

-- --------------------------------------------------------

--
-- Table structure for table `productos_prorroga`
--

CREATE TABLE `productos_prorroga` (
  `id_pp` int(11) NOT NULL,
  `id_prorroga` int(11) NOT NULL,
  `id_propuesta` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productos_prorroga`
--

INSERT INTO `productos_prorroga` (`id_pp`, `id_prorroga`, `id_propuesta`) VALUES
(30, 39, 25);

-- --------------------------------------------------------

--
-- Table structure for table `prorrogas`
--

CREATE TABLE `prorrogas` (
  `id_prorroga` int(11) NOT NULL,
  `id_acto` int(11) NOT NULL,
  `cantidad_dias` int(11) NOT NULL,
  `tipo_dias` varchar(20) NOT NULL,
  `fecha_emision` date NOT NULL,
  `fecha_vencimiento` date NOT NULL,
  `fecha_entrega` date NOT NULL,
  `fecha_respuesta` date NOT NULL,
  `motivo` text NOT NULL,
  `estado` int(11) NOT NULL,
  `dirigidoa` text NOT NULL,
  `cargoa` text NOT NULL,
  `dias_aprovados` int(11) NOT NULL,
  `tipo_dia` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `renglones_acto`
--

CREATE TABLE `renglones_acto` (
  `id_renglon` int(11) NOT NULL,
  `id_acto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `codigo` text NOT NULL,
  `descripcion` text NOT NULL,
  `unidad_medida` text NOT NULL,
  `clasificacion` text NOT NULL,
  `nro_renglon` int(11) NOT NULL,
  `estado_entrega` text NOT NULL,
  `unidades_pendiente` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `renglones_acto`
--

INSERT INTO `renglones_acto` (`id_renglon`, `id_acto`, `cantidad`, `codigo`, `descripcion`, `unidad_medida`, `clasificacion`, `nro_renglon`, `estado_entrega`, `unidades_pendiente`) VALUES
(29, 38, 5, '95101011', 'PIEZA EQUIPO VENTILADORES ', 'unidad', 'Equipo-Accesorios-Suministros Medicos', 2, '', 5),
(28, 38, 6, '95101011', 'PIEZA DE EQUIPO VENTILADORES', 'unidad', 'Equipo-Accesorios-Suministros Medicos', 1, '', 6),
(27, 37, 1, '95101010', 'SUMINISTRO, INSTALACION, ENTRENAMIENTO DE UNA CENTRIFUGA', 'unidad', 'Equipo-Accesorios-Suministros Medicos', 1, '', 1),
(13, 23, 1, '95101011', '	EQUIPO PARA LA MEDICIÓN DEL GASTO DE ENERGÍA DURANTE LA MARCHA', 'unidad', 'Equipo-Accesorios-Suministros Médicos', 1, '', 1),
(14, 24, 1, '95101011', 'Centrifuga de 24 posiciones para botellas babcock.', 'unidad', 'Equipo-Accesorios-Suministros Medicos', 1, '', 1),
(15, 26, 1, '95101011', 'BICICLETA ESTATICA SIN RESPALDAR', 'unidad', 'Equipo-Accesorios-Suministros Medicos', 1, '', 1),
(16, 27, 20, '95101011', 'ATRIL DE METAL DE 2 GANCHOS Y 2 RUEDAS ', 'unidad', 'Equipo-Accesorios-Suministros Medicos', 6, '', 20),
(17, 27, 20, '95101011', 'MESA PARA COMER PARA PACIENTES', 'unidad', 'Equipo-Accesorios-Suministros Medicos', 8, '', 20),
(18, 28, 1, '41103809', 'EQUIPO DE HEMAOLOGIA VETERINARIO', 'unidad', 'Equipo-Accesorios-Suministros Medicos', 1, '', 1),
(19, 29, 10, '95101011', 'CARRO DE MEDICACION PARA UNIDOSIS', 'unidad', 'Equipo-Accesorios-Suministros Medicos', 1, '', 10),
(20, 30, 1, '95101010', 'SET DE MASAS PARA CALIBRAR BALANZAS CLASE F1', 'unidad', 'Equipo-Accesorios-Suministros Medicos', 1, '', 1),
(21, 31, 1, '95101010', 'SUMINISTRO , INSTALACION Y ENTRENAMIENTO DE 1 PURIFICADOR DE AGUA TIPO 1', 'unidad', 'Equipo-Accesorios-Suministros Medicos', 1, '', 1),
(22, 32, 100, '95101011', 'HOJA DE PROPILENO', 'unidad', 'Equipo-Accesorios-Suministros Medicos', 1, '', 100),
(26, 36, 2, '95101010', 'CONGELADORES DE LABORATORIO', 'unidad', 'Equipo-Accesorios-Suministros Medicos', 1, '', 2),
(24, 34, 1000, '95101011', 'PROTECTOR OCULAR PARA FOTOTERAPIA', 'unidad', 'Equipo-Accesorios-Suministros Medicos', 1, '', 1000),
(25, 35, 1, '95101011', 'BALANZA DIGITAL ANALITICA DE ALTA RESOLUCION CON GRADILLA COLGANTE', 'unidad', 'Equipo-Accesorios-Suministros Medicos', 1, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tipo_lp`
--

CREATE TABLE `tipo_lp` (
  `id_tlp` int(11) NOT NULL,
  `Nombre` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tipo_lp`
--

INSERT INTO `tipo_lp` (`id_tlp`, `Nombre`) VALUES
(1, 'Global'),
(2, 'Por renglon');

-- --------------------------------------------------------

--
-- Table structure for table `tipo_pago`
--

CREATE TABLE `tipo_pago` (
  `id_tipop` int(11) NOT NULL,
  `nombre` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tipo_pago`
--

INSERT INTO `tipo_pago` (`id_tipop`, `nombre`) VALUES
(1, 'Efectivo'),
(2, 'Transferencia'),
(3, 'Cheque'),
(4, 'Deposito'),
(5, 'TDC/TDD');

-- --------------------------------------------------------

--
-- Table structure for table `tipo_producto`
--

CREATE TABLE `tipo_producto` (
  `id_tipop` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tipo_producto`
--

INSERT INTO `tipo_producto` (`id_tipop`, `nombre`) VALUES
(1, 'Mobiliario'),
(2, 'Equipo'),
(3, 'Repuesto'),
(4, 'Accesorio'),
(5, 'Descartable'),
(6, 'Material de laboratorio'),
(7, 'Otros'),
(8, 'Mantenimiento y calibracion');

-- --------------------------------------------------------

--
-- Table structure for table `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `id_tipo` int(11) NOT NULL,
  `nombre` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`id_tipo`, `nombre`) VALUES
(0, 'Super Usuario'),
(1, 'Gerencial'),
(2, 'Administrativo'),
(3, 'Ventas'),
(4, 'Gestion');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `usuario` varchar(32) NOT NULL,
  `password` text NOT NULL,
  `level` int(11) NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre`, `usuario`, `password`, `level`, `estado`) VALUES
(1, 'Administrador Sistema', 'admsys', '$2y$10$IQ1tNjhWxgCagCSwJtVa7uqcGRBydnpvbsOHJ9RwqetTLV2C1FIgO', 0, 1),
(4, 'Jose Vicente Fasquias', 'jvfasquias', '$2y$10$DRCVqJEwEwJ5mcMZYgp/YuSfCsPtzLND86ubSRoiRIbueCCUyQg2.', 3, 1),
(5, 'Anahis Medina', 'amedina', '$2y$10$wbmvkzDj6nJC.xlA6Tp3guqBOszficXhbC99IlwzbZB.s/CmjpbPm', 1, 1),
(6, 'Jonathan Zamora', 'jzamora', '$2y$10$bP7vOxYDxW.9NgZ4PLOajOLZt5PDxMZV7EGSyCeV5C/yvWzikRWvi', 4, 1),
(7, 'Antonio Isaac', 'aisaac', '$2y$10$GNtzxVhQu9G2hEntx65c8ebuLDja0h89cv9QZv.wbaKlgpVa3a9FG', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `utiles`
--

CREATE TABLE `utiles` (
  `cedula` int(22) NOT NULL DEFAULT '0',
  `valor` decimal(15,2) DEFAULT NULL,
  `concepto` varchar(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ventas_ip`
--

CREATE TABLE `ventas_ip` (
  `id_venta` int(11) NOT NULL,
  `id_inst` int(11) NOT NULL,
  `nro_factura` text NOT NULL,
  `fecha` date NOT NULL,
  `termino_pago` text NOT NULL,
  `dias_credito` int(11) NOT NULL,
  `subtotal` decimal(50,2) NOT NULL,
  `tax` decimal(50,2) NOT NULL,
  `total` decimal(50,2) NOT NULL,
  `vendedor` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actas_adjudicacion`
--
ALTER TABLE `actas_adjudicacion`
  ADD PRIMARY KEY (`id_acta`);

--
-- Indexes for table `actas_decision`
--
ALTER TABLE `actas_decision`
  ADD PRIMARY KEY (`id_decision`);

--
-- Indexes for table `actos`
--
ALTER TABLE `actos`
  ADD PRIMARY KEY (`id_acto`);

--
-- Indexes for table `calificaciones`
--
ALTER TABLE `calificaciones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id_compra`);

--
-- Indexes for table `detalle_compra`
--
ALTER TABLE `detalle_compra`
  ADD PRIMARY KEY (`id_detalle_c`);

--
-- Indexes for table `detalle_entrega`
--
ALTER TABLE `detalle_entrega`
  ADD PRIMARY KEY (`id_detalle`);

--
-- Indexes for table `detalle_ventas_ip`
--
ALTER TABLE `detalle_ventas_ip`
  ADD PRIMARY KEY (`id_detalle`);

--
-- Indexes for table `documentos_prorrogas`
--
ALTER TABLE `documentos_prorrogas`
  ADD PRIMARY KEY (`id_documento`);

--
-- Indexes for table `entregas`
--
ALTER TABLE `entregas`
  ADD PRIMARY KEY (`id_entrega`);

--
-- Indexes for table `estado_licitaciones`
--
ALTER TABLE `estado_licitaciones`
  ADD PRIMARY KEY (`id_status`);

--
-- Indexes for table `estado_prorogas`
--
ALTER TABLE `estado_prorogas`
  ADD PRIMARY KEY (`id_estado`);

--
-- Indexes for table `facturas_actos`
--
ALTER TABLE `facturas_actos`
  ADD PRIMARY KEY (`id_factura`);

--
-- Indexes for table `fdc`
--
ALTER TABLE `fdc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fde`
--
ALTER TABLE `fde`
  ADD PRIMARY KEY (`id_fde`);

--
-- Indexes for table `feriados`
--
ALTER TABLE `feriados`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `informes_recepcion`
--
ALTER TABLE `informes_recepcion`
  ADD PRIMARY KEY (`id_informe`);

--
-- Indexes for table `instituciones`
--
ALTER TABLE `instituciones`
  ADD PRIMARY KEY (`id_inst`);

--
-- Indexes for table `licitaciones`
--
ALTER TABLE `licitaciones`
  ADD PRIMARY KEY (`id_licitacion`);

--
-- Indexes for table `multas`
--
ALTER TABLE `multas`
  ADD PRIMARY KEY (`id_multa`);

--
-- Indexes for table `odc_acto`
--
ALTER TABLE `odc_acto`
  ADD PRIMARY KEY (`id_odc`);

--
-- Indexes for table `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`id_pago`);

--
-- Indexes for table `participantes`
--
ALTER TABLE `participantes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `presentacion_cuenta`
--
ALTER TABLE `presentacion_cuenta`
  ADD PRIMARY KEY (`id_pdc`);

--
-- Indexes for table `privilegios_usuarios`
--
ALTER TABLE `privilegios_usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indexes for table `productos_participacion`
--
ALTER TABLE `productos_participacion`
  ADD PRIMARY KEY (`id_participacion`);

--
-- Indexes for table `productos_prorroga`
--
ALTER TABLE `productos_prorroga`
  ADD PRIMARY KEY (`id_pp`);

--
-- Indexes for table `prorrogas`
--
ALTER TABLE `prorrogas`
  ADD PRIMARY KEY (`id_prorroga`);

--
-- Indexes for table `renglones_acto`
--
ALTER TABLE `renglones_acto`
  ADD PRIMARY KEY (`id_renglon`);

--
-- Indexes for table `tipo_lp`
--
ALTER TABLE `tipo_lp`
  ADD PRIMARY KEY (`id_tlp`);

--
-- Indexes for table `tipo_pago`
--
ALTER TABLE `tipo_pago`
  ADD PRIMARY KEY (`id_tipop`);

--
-- Indexes for table `tipo_producto`
--
ALTER TABLE `tipo_producto`
  ADD PRIMARY KEY (`id_tipop`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indexes for table `utiles`
--
ALTER TABLE `utiles`
  ADD PRIMARY KEY (`cedula`);

--
-- Indexes for table `ventas_ip`
--
ALTER TABLE `ventas_ip`
  ADD PRIMARY KEY (`id_venta`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actas_adjudicacion`
--
ALTER TABLE `actas_adjudicacion`
  MODIFY `id_acta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `actas_decision`
--
ALTER TABLE `actas_decision`
  MODIFY `id_decision` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `actos`
--
ALTER TABLE `actos`
  MODIFY `id_acto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `calificaciones`
--
ALTER TABLE `calificaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `compras`
--
ALTER TABLE `compras`
  MODIFY `id_compra` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `detalle_compra`
--
ALTER TABLE `detalle_compra`
  MODIFY `id_detalle_c` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `detalle_entrega`
--
ALTER TABLE `detalle_entrega`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `detalle_ventas_ip`
--
ALTER TABLE `detalle_ventas_ip`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `documentos_prorrogas`
--
ALTER TABLE `documentos_prorrogas`
  MODIFY `id_documento` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `entregas`
--
ALTER TABLE `entregas`
  MODIFY `id_entrega` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `estado_licitaciones`
--
ALTER TABLE `estado_licitaciones`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `estado_prorogas`
--
ALTER TABLE `estado_prorogas`
  MODIFY `id_estado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `facturas_actos`
--
ALTER TABLE `facturas_actos`
  MODIFY `id_factura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fdc`
--
ALTER TABLE `fdc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `fde`
--
ALTER TABLE `fde`
  MODIFY `id_fde` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `feriados`
--
ALTER TABLE `feriados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `informes_recepcion`
--
ALTER TABLE `informes_recepcion`
  MODIFY `id_informe` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `instituciones`
--
ALTER TABLE `instituciones`
  MODIFY `id_inst` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `licitaciones`
--
ALTER TABLE `licitaciones`
  MODIFY `id_licitacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `multas`
--
ALTER TABLE `multas`
  MODIFY `id_multa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `odc_acto`
--
ALTER TABLE `odc_acto`
  MODIFY `id_odc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `pagos`
--
ALTER TABLE `pagos`
  MODIFY `id_pago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `participantes`
--
ALTER TABLE `participantes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `presentacion_cuenta`
--
ALTER TABLE `presentacion_cuenta`
  MODIFY `id_pdc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `productos_participacion`
--
ALTER TABLE `productos_participacion`
  MODIFY `id_participacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `productos_prorroga`
--
ALTER TABLE `productos_prorroga`
  MODIFY `id_pp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `prorrogas`
--
ALTER TABLE `prorrogas`
  MODIFY `id_prorroga` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `renglones_acto`
--
ALTER TABLE `renglones_acto`
  MODIFY `id_renglon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `tipo_lp`
--
ALTER TABLE `tipo_lp`
  MODIFY `id_tlp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tipo_pago`
--
ALTER TABLE `tipo_pago`
  MODIFY `id_tipop` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tipo_producto`
--
ALTER TABLE `tipo_producto`
  MODIFY `id_tipop` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ventas_ip`
--
ALTER TABLE `ventas_ip`
  MODIFY `id_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
