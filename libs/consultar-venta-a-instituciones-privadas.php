
<?php 
	include ('libs/conexion.php');
	$fde='';
	$recibido='';
	$id_fde=$_POST['id'];
	$query_p=mysqli_query($conexion, "SELECT a.fecha_factura, a.nro_factura, a.subtotal, a.tax, a.total, a.id_inst, c.nombre, c.direccion, c.telefono FROM fde a, actos b, instituciones c WHERE id_fde='$id_fde' AND b.id_acto=a.id_acto AND c.id_inst=b.id_inst") or die (mysqli_error($conexion));
	$array_p=mysqli_fetch_array($query_p);

	

		if($array_p['ubicacion_emision']!=''){
			$fde=$array_p['ubicacion_emision'];			
		}

		if($array_p['ubicacion_recibido']!=''){
			$recibido=$array_p['ubicacion_recibido'];
		}




?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="frames.php?p=inicio"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Consulta de fianza de endoso</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Consulta de fianza de endoso</h1>
			</div>
		</div><!--/.row-->
					<?php include("libs/actos_prorroga.php"); ?>
	<style type="text/css">
		.slcall{
			display: none;
		}
	</style>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md-6">	
							<div class="panel-heading" style="margin-bottom: 10px;">Acto</div>
								<div class="form-group" id="vdirigido">
									<label>Nro de factura</label>
								</div>
								<div class="form-group" id="vcargo">
									<p><?php echo $array_p['nro_factura']; ?></p>
								</div>
								<div class="form-group" id="vdirigido">
									<label>Instituci&oacute;n</label>
								</div>
								<div class="form-group" id="vcargo">
									<p><?php echo $array_p['nombre']; ?></p>
								</div>
								<div class="form-group" id="vdirigido">
									<label>Tel&eacute;fono</label>
								</div>
								<div class="form-group" id="vcargo">
									<p><?php echo $array_p['telefono']; ?></p>
								</div>
							</div>
						<div class="col-md-6">	
							<div class="panel-heading" style="margin-bottom: 10px;">Archivos referentes</div>
								<div class="form-group" id="vdirigido">
									<label>Fecha de facturaci&oacute;n:</label>
								</div>
								<div class="form-group" id="vcargo">									
									<p><?php echo date('m-d-Y', strtotime($array_p['fecha_facturacion'])); ?></p>		
								</div>
								<div class="form-group" id="vdirigido">
									<label>Direcci&oacute;n</label>
								</div>
								<div class="form-group" id="vcargo">
									<p><?php echo $array_p['direccion']; ?></p>								
								</div>					
							</div>
						</div>
					</div>
				</div><!-- /.col-->
			</div><!-- /.row -->
	</div><!--/.main-->

  <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/common.js"></script>
  <script src="js/datepicker.js"></script>
  <script src="js/datepicker.es-ES.js"></script>
  <script src="js/main.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/bootstrap-table.js"></script>	
	<script>

		function mostrar_prorroga(){

			window.open('<?php echo $array_p["ubicacion_solicitud"];?>','popup','toolbars=no, directories=no,menubar=no,status=no,resiz eable=0,width=800,height=900');

		}

		function mostrar_respuesta(){

			var direccion='<?php echo $fde;?>';

			if(direccion.length>0){
				window.open(''+direccion+'','popup','toolbars=no, directories=no,menubar=no,status=no,resiz eable=0,width=800,height=900');
			}else{
				alert("El documento no ha sido emitido aun");
			}	
		}

		function mostrar_recibido(){
			var direccion='<?php echo $recibido;?>';
			if(direccion.length>0){
				window.open(''+direccion+'','popup','toolbars=no, directories=no,menubar=no,status=no,resiz eable=0,width=800,height=900');
			}else{
				alert("El documento no ha sido entregado aun.");
			}	
		}
	</script>	