<?php
	include("conexion.php");
	$querylist=mysqli_query($conexion, "SELECT a.id_prorroga, a.id_acto, a.fecha_entrega, a.fecha_vencimiento, a.fecha_respuesta, a.estado, a.fecha_emision, b.id_inst, c.nombre as estado, d.nombre as instituto, e.nro as odc FROM prorrogas a, actos b, estado_prorogas c, instituciones d, odc_acto e WHERE a.id_acto=b.id_acto AND b.id_inst=d.id_inst AND c.id_estado=a.estado AND e.id_acto=a.id_acto") or die (mysqli_error($conexion));
	$resultlist=mysqli_num_rows($querylist);	
   

	$l=1;
    $listado_prorrogas='[]';
	while($l<=$resultlist){
        $arraylist=mysqli_fetch_array($querylist);
            if($arraylist['fecha_entrega']!='0000-00-00'){
                $fechaentrega=date_format(date_create($arraylist['fecha_entrega']), "m-d-Y");
            }else{
                $fechaentrega='NULL';
            }

            if($arraylist['fecha_respuesta']!='0000-00-00'){
                $fecharespuesta=date_format(date_create($arraylist['fecha_respuesta']), "m-d-Y");
            }else{
                $fecharespuesta='NULL';
            }
		if($l==1){
			$listado_prorrogas ='[';
		}
    	if($l!=$resultlist){    		
			$listado_prorrogas .= '{
        			"id": '.$arraylist['id_prorroga'].',
                    "fecha_em": "'.date_format(date_create($arraylist['fecha_emision']), "m-d-Y").'",
                    "fecha_vto": "'.date_format(date_create($arraylist['fecha_vencimiento']), "m-d-Y").'",
                    "fecha_ent": "'.$fechaentrega.'",
                    "fecha_resp": "'.$fecharespuesta.'",
                    "odc": "'.$arraylist['odc'].'",
        			"inst": "'.$arraylist['instituto'].'",
        			"status": "'.$arraylist['estado'].'"
    			},';
    	}else{
    		$listado_prorrogas .= '{
                    "id": '.$arraylist['id_prorroga'].',
                    "fecha_em": "'.date_format(date_create($arraylist['fecha_emision']), "m-d-Y").'",
                    "fecha_vto": "'.date_format(date_create($arraylist['fecha_vencimiento']), "m-d-Y").'",
                    "fecha_ent": "'.$fechaentrega.'",
                    "fecha_resp": "'.$fecharespuesta.'",
                    "odc": "'.$arraylist['odc'].'",
                    "inst": "'.$arraylist['instituto'].'",
                    "status": "'.$arraylist['estado'].'"
    			}]';
    	}		
    	$l++;		
 	}

?>