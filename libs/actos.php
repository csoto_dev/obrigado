<?php
	include("conexion.php");

	$querylist=mysqli_query($conexion, "SELECT a.n_acto, a.id_acto, DATE_FORMAT(a.fecha_publicacion, '%m-%d-%Y') AS fecha_publicacion, DATE_FORMAT(a.fecha_acto, '%m-%d-%Y') AS fecha_acto, a.id_status, a.id_inst, a.calificacion, b.nombre as instituto, c.nombre as status, d.nombre as calificacion, e.Nombre as tipo FROM actos a, instituciones b, estado_licitaciones c, calificaciones d, tipo_lp e WHERE a.id_status=c.id_status AND a.id_inst=b.id_inst AND a.calificacion=d.id AND a.tipo_acto=e.id_tlp") or die (mysqli_error($conexion));
	$resultlist=mysqli_num_rows($querylist);	
	$l=1;
    $listado_actos= '[]';
	while($l<=$resultlist){
		$arraylist=mysqli_fetch_array($querylist);
		if($l==1){
			$listado_actos= '[';
		}
    	if($l!=$resultlist){    		
			$listado_actos .= '{
        			"id_acto": '.$arraylist['id_acto'].',
        			"n_acto": "'.$arraylist['n_acto'].'",
        			"fecha_publicacion": "'.$arraylist['fecha_publicacion'].'",
        			"fecha_acto": "'.$arraylist['fecha_acto'].'",
        			"instituto": "'.utf8_encode($arraylist['instituto']).'",
        			"status": "'.$arraylist['status'].'",
        			"tipo": "'.$arraylist['tipo'].'",
        			"calificacion": "'.$arraylist['calificacion'].'"
    			},';
    	}else{
    		$listado_actos .='{
        			"id_acto": '.$arraylist['id_acto'].',
        			"n_acto": "'.$arraylist['n_acto'].'",
        			"fecha_publicacion": "'.$arraylist['fecha_publicacion'].'",
        			"fecha_acto": "'.$arraylist['fecha_acto'].'",
        			"instituto": "'.utf8_encode($arraylist['instituto']).'",
        			"status": "'.$arraylist['status'].'",
        			"tipo": "'.$arraylist['tipo'].'",
        			"calificacion": "'.$arraylist['calificacion'].'"
    			}]';
    	}		
    	$l++;		
 	}

?>