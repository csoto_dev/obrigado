	<?php
		$year=date('Y');
		$feriados = array('1-01','9-01', 'Carnaval', 'viernes santo', '1-05', '3-11', '5-11', '10-11', '28-11', '8-12', '25-12');
		$feriadosano = array();
		$domingresurreccion=date("Y-m-d", easter_date($year));
		for ($i=0; $i < 11; $i++) { 
			switch ($feriados[$i]) {
				case 'Carnaval':
					$carnaval = date('Y-m-d', strtotime ('-47 day', strtotime( $domingresurreccion )));					
					$feriadosano[$i]=$carnaval;	
					break;

				case 'viernes santo':
					$viernessanto = date('Y-m-d', strtotime ('-2 day', strtotime($domingresurreccion)));	
					$feriadosano[$i]=$viernessanto;		
					break;
				
				default:
					$fecha=$feriados[$i]."-".$year;
					$dia=date_format(date_create($fecha), "l");
					switch ($dia) {
						case 'Saturday':
							$feriado=date('Y-m-d', strtotime('+2 day', strtotime($fecha))); 
							break;

						case 'Sunday':
							$feriado=date('Y-m-d', strtotime('+1 day', strtotime($fecha))); 
							break;
						
						default:
							$feriado=date('Y-m-d', strtotime($fecha));
							break;
					}
					$feriadosano[$i]=$feriado;
					break;
			}
			
			
		}?>