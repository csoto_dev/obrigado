<?php
require('libs/conexion.php');
require('libs/fpdf.php');
$query_main=mysqli_query($conexion, "SELECT a.n_acto, a.id_acto, a.monto_act, DATE_FORMAT(a.fecha_publicacion, '%m-%d-%Y') AS fecha_publicacion, DATE_FORMAT(a.fecha_acto, '%m-%d-%Y') AS fecha_acto,  b.nombre as instituto, c.nombre as status, d.nombre as calificacion, e.Nombre as tipo FROM actos a, instituciones b, estado_licitaciones c, calificaciones d, tipo_lp e WHERE a.id_status=c.id_status AND a.id_inst=b.id_inst AND a.calificacion=d.id AND a.tipo_acto=e.id_tlp") or die (mysqli_error($conexion));


$dia=date('d');
$mes_eng=date('F');
$ano=date('Y');
$meses_esp=array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
$meses_eng=array('January','February','March','April','May','June','July','August','September','October','November','December');
$mes_esp=str_replace($meses_eng, $meses_esp, $mes_eng);

class PDF extends FPDF
{
    // Cabecera de página
    function Header()
    {
        // Logo
        $this->Image('logo.png',10,8,50);
        // Arial bold 15
        $this->SetFont('Arial','I',12);
        // Movernos a la derecha
        $this->Ln(20);
    }

    // Pie de página
    function Footer()
    {
        // Posición: a 1,5 cm del final
        $this->SetY(-20);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Número de página
        $this->Cell(0,5,utf8_decode('DIRECCIÓN: SAN FRANCISCO, CALLE ISAAC HANONO, OCEANIA BUSINESS PLAZA, TORRE 3000, PISO 6. TELÉFONO: (+507)387-6392 / (+507)387-6393. E-MAIL: INFO@OBRIGADOMEDICALGROUP.COM.'),0,1,'C');

        //$this->Cell(0,5,'TELÉFONO: (+507)387-6392 / (+507)387-6393. E-MAIL: INFO@OBRIGADOMEDICALGROUP.COM.',0,1,'C');
        $this->Cell(0,5,'WEBSITE: HTTPS://WWW.OBRIGADOMEDICALGROUP.COM',0,1,'C');
    }
    var $widths;
    var $aligns;

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'C';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}

// Creación del objeto de la clase heredada
$pdf = new pdf("L","mm","Legal");
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',12);
$pdf->Cell(0,7,utf8_decode('Panamá, '.$dia.' de '.$mes_esp.' de '.$ano),0,1, 'R');
$pdf->ln(10);
$pdf->SetFont('Arial','B',18);
$pdf->Cell(0,7,'Informe de actos participados',0,1, 'C');
//linea 2
$pdf->ln(7);
$pdf->SetFont('Arial','B',10);
$pdf->SetFillColor(27,117,186);
$pdf->Cell(50,7, utf8_decode('Código LP'),1,0, 'C',true);
$pdf->Cell(34,7, utf8_decode('Fecha publicación'),1,0, 'C',true);
$pdf->Cell(34,7, utf8_decode('Fecha apertura'),1,0, 'C',true);
$pdf->Cell(30,7, utf8_decode('Tipo LP'),1,0, 'C',true);
$pdf->Cell(50,7, utf8_decode('Institución'),1,0, 'C',true);
$pdf->Cell(35,7, utf8_decode('Monto acto'),1,0, 'C',true);
$pdf->Cell(35,7, utf8_decode('Monto propuesta'),1,0, 'C',true);
$pdf->Cell(35,7, utf8_decode('Estado'),1,0, 'C',true);
$pdf->Cell(35,7, utf8_decode('Calificación'),1,0, 'C',true);
//$pdf->SetFont('Arial','B',10);
$pdf->SetWidths(array(50,34,34,30,50,35,35,35,35));
//$pdf->Row(array(utf8_decode('Código LP'),utf8_decode('Fecha publicación'),utf8_decode('Fecha apertura'),utf8_decode('Tipo LP'),utf8_decode('Institución'),utf8_decode('Monto acto'),utf8_decode('Monto propuesta'),utf8_decode('Estado'),utf8_decode('Calificación')));
$pdf->SetFont('Arial','',10);

$pdf->ln(7);
while ($array_main=mysqli_fetch_array($query_main)) {

    $querytotalp=mysqli_query($conexion, "SELECT SUM(monto_total) as total_propuesta FROM productos_participacion WHERE id_acto='".$array_main['id_acto']."'") or die (mysqli_error($conexion));
    $arraytotalp=mysqli_fetch_array($querytotalp);
    $pdf->Row(array(utf8_decode($array_main['n_acto']),utf8_decode($array_main['fecha_publicacion']),utf8_decode($array_main['fecha_acto']),utf8_decode($array_main['tipo']),utf8_decode($array_main['instituto']),utf8_decode($array_main['monto_act']),utf8_decode($arraytotalp['total_propuesta']),utf8_decode($array_main['status']),utf8_decode($array_main['calificacion'])));
}

$pdf->Output('', 'prorroga-acto.pdf','');
?>
