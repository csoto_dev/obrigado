<?php 
	$id_acto=$_POST['id_acto'];
	if(isset($_POST['id_renglon'])){
		$id_renglon=$_POST['id_renglon'];
	}
	$id_renglon=$_POST['id_renglon'];
	$url=$_POST['url'];
    
	$busqueda=$_POST['busqueda'];	
	include("libs/resultado-busqueda.php"); 
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>.:Obrigado Licitaciones:.</title>


<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/bootstrap-table.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">

<!--Icons-->
<script src="js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

<style type="text/css">
		.slcall{
			display: none;
		}
	</style>

</head>

<body style="padding: 0;">			
		<form name="formbusqueda" action="" method="post">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Productos</div>
					<div class="panel-body">
						<table id="table"   data-show-agregar="true"  data-show-regresar="true" data-show-procesar="true" data-pagination="true" data-select-item-name="id_producto"  data-sort-name="name" data-sort-order="asc">
						    <thead>
						    <tr>                          
						        <th data-field="id" data-checkbox="true" >Item ID</th>
						        <th data-field="codigo" data-sortable="true">C&oacute;digo</th>
						        <th data-field="descripcion"  data-sortable="true">Descripci&oacute;n</th>
						        <th data-field="marca" data-sortable="true">Marca</th>
						        <th data-field="unidadmedida"  data-sortable="true">Unidad med.</th>
						        <th data-field="tipo" data-sortable="true">Tipo de producto</th>
						       
						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->
		<?php 
			if(isset($_POST['id_renglon'])){
				echo" <input type='hidden' name='id_renglon' value='$id_renglon'>";
			} 
		?>
										
		</form>
		
		
		
	</div><!--/.main-->
	<form name="registroprod" action="producto-nuevo.php" method="post">
		<input type="hidden" name="id_acto" value="<?php echo $id_acto;?>">
		<input type="hidden" name="id_renglon" value="<?php echo $id_renglon;?>">
		<input type="hidden" name="url" value="<?php echo $url;?>">
		<input type="hidden" name="busqueda" value="<?php echo $busqueda;?>">
	</form>
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/bootstrap-table.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>
	<script >
		$(document).on('change', "input[type='checkbox']" , function(){
  
		  // in the handler, 'this' refers to the box clicked on
 		var $box = $(this);
		 if ($box.is(":checked")) {
		    // the name of the box is retrieved using the .attr() method
		    // as it is assumed and expected to be immutable
		    var group = "input:checkbox[name='" + $box.attr("name") + "']";
		    // the checked state of the group/box on the other hand will change
		    // and the current value is retrieved using .prop() method
		    $(group).prop("checked", false);
		    $box.prop("checked", true);
		  } else {
		    $box.prop("checked", false);
		  }
		});
	</script>	
	<script>
		var mydata= <?php echo $var;?>;
		$(function () {
		    $('#table').bootstrapTable({
		        data: mydata
		    });
		});
	    $(document).keyup(function(event){
	        if(event.which==27){
				window.close();
	        }
	    });

		function registrar(){			
			document.registroprod.submit();
		}

	    function retroceso() {
			window.location= "<?php echo $url?>?id=<?php echo $id_acto?>";
	    }
		
		function procesarLP() {
			if ($('input[type=checkbox]').is(':checked') ) {
				document.formbusqueda.action= "<?php echo $url?>?id=<?php echo $id_acto?>";
				document.formbusqueda.submit();
			}else{
				alert('Debe seleccionar un producto.');
			}
	    }
		
	</script>	

</body>

</html>
