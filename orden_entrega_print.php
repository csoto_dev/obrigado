<?php
require('libs/conexion.php');
require('libs/fpdf.php');
$id_entrega=$_GET['id'];
$query_main=mysqli_query($conexion, "SELECT a.id_acto, a.fecha_entrega, a.direccion,  c.termino_pago, a.tipo_entrega, b.nombre as institucion, c.id_inst, c.n_req, d.nro as odc FROM entregas a, instituciones b, actos c, odc_acto d WHERE id_entrega='$id_entrega' AND c.id_acto=a.id_acto AND b.id_inst=c.id_inst AND d.id_acto=a.id_acto ") or die (mysqli_error($conexion));
$array_main=mysqli_fetch_assoc($query_main);

$instututo=$array_main['institucion'];
$odc=$array_main['odc'];
$req=$array_main['n_req'];
$fecha_emision=$array_main['fecha_entrega'];
$tipo_entrega=$array_main['tipo_entrega'];
$termino_pago=$array_main['termino_pago'];
$direccion=$array_main['direccion'];

$dia=date('d', strtotime($fecha_emision));
$mes_eng=date('F', strtotime($fecha_emision));
$ano=date('Y', strtotime($fecha_emision));
$meses_esp=array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
$meses_eng=array('January','February','March','April','May','June','July','August','September','October','November','December');
$mes_esp=str_replace($meses_eng, $meses_esp, $mes_eng);

class PDF extends FPDF
{
	// Cabecera de página
	function Header()
	{
		// Logo
		$this->Image('logo.png',10,8,50);
		// Arial bold 15
		$this->SetFont('Arial','I',12);
		// Movernos a la derecha
		$this->Ln(20);
	}

	// Pie de página
	function Footer()
	{
		// Posición: a 1,5 cm del final
		$this->SetY(-25);
		// Arial italic 8
		$this->SetFont('Arial','I',8);
		// Número de página
		$this->Cell(0,5,'DIRECCIÓN: SAN FRANCISCO, CALLE ISAAC HANONO, OCEANIA BUSINESS PLAZA, TORRE 3000, PISO 6.',0,1,'C');

		$this->Cell(0,5,'TELÉFONO: (+507)387-6392 / (+507)387-6393. E-MAIL: INFO@OBRIGADOMEDICALGROUP.COM.',0,1,'C');
		$this->Cell(0,5,'WEBSITE: HTTPS://WWW.OBRIGADOMEDICALGROUP.COM',0,1,'C');
	}
	var $widths;
	var $aligns;

	function SetWidths($w)
	{
	    //Set the array of column widths
	    $this->widths=$w;
	}

	function SetAligns($a)
	{
	    //Set the array of column alignments
	    $this->aligns=$a;
	}

	function Row($data)
	{
	    //Calculate the height of the row
	    $nb=0;
	    for($i=0;$i<count($data);$i++)
	        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
	    $h=5*$nb;
	    //Issue a page break first if needed
	    $this->CheckPageBreak($h);
	    //Draw the cells of the row
	    for($i=0;$i<count($data);$i++)
	    {
	        $w=$this->widths[$i];
	        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
	        //Save the current position
	        $x=$this->GetX();
	        $y=$this->GetY();
	        //Draw the border
	        $this->Rect($x,$y,$w,$h);
	        //Print the text
	        $this->MultiCell($w,5,$data[$i],0,$a);
	        //Put the position to the right of the cell
	        $this->SetXY($x+$w,$y);
	    }
	    //Go to the next line
	    $this->Ln($h);
	}

	function CheckPageBreak($h)
	{
	    //If the height h would cause an overflow, add a new page immediately
	    if($this->GetY()+$h>$this->PageBreakTrigger)
	        $this->AddPage($this->CurOrientation);
	}

	function NbLines($w,$txt)
	{
	    //Computes the number of lines a MultiCell of width w will take
	    $cw=&$this->CurrentFont['cw'];
	    if($w==0)
	        $w=$this->w-$this->rMargin-$this->x;
	    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
	    $s=str_replace("\r",'',$txt);
	    $nb=strlen($s);
	    if($nb>0 and $s[$nb-1]=="\n")
	        $nb--;
	    $sep=-1;
	    $i=0;
	    $j=0;
	    $l=0;
	    $nl=1;
	    while($i<$nb)
	    {
	        $c=$s[$i];
	        if($c=="\n")
	        {
	            $i++;
	            $sep=-1;
	            $j=$i;
	            $l=0;
	            $nl++;
	            continue;
	        }
	        if($c==' ')
	            $sep=$i;
	        $l+=$cw[$c];
	        if($l>$wmax)
	        {
	            if($sep==-1)
	            {
	                if($i==$j)
	                    $i++;
	            }
	            else
	                $i=$sep+1;
	            $sep=-1;
	            $j=$i;
	            $l=0;
	            $nl++;
	        }
	        else
	            $i++;
	    }
	    return $nl;
	}
}

	// Creación del objeto de la clase heredada
	$pdf = new pdf();
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->SetFont('Arial','',12);
	$pdf->Cell(0,7,utf8_decode('Panamá, '.$dia.' de '.$mes_esp.' de '.$ano),0,1, 'R');
	$pdf->ln(10);
	$pdf->SetFont('Arial','B',18);
	$pdf->Cell(0,7,'Nota de entrega',0,1, 'C');
	$pdf->ln(10);
	//linea 1
	$pdf->SetFont('Arial','B',12);
	$pdf->Cell(60,7,'Ref. nota de entrega:',0,0, 'L');
	$pdf->Cell(75,7,'Tipo de entrega:',0,0, 'L');
	$pdf->Cell(75,7,'Termino de pago:',0,1, 'L');
	$pdf->SetFont('Arial','',12);
	$pdf->Cell(60,7, $id_entrega ,0,0, 'L');
	$pdf->Cell(75,7, $tipo_entrega,0,0, 'L');
	$pdf->Cell(75,7, $termino_pago,0,1, 'L');
	//linea 2
	$pdf->SetFont('Arial','B',12);
	$pdf->Cell(65,7,'Nro. de orden de compra:',0,0, 'L');
	$pdf->Cell(125,7,'Cliente:',0,1, 'L');
	$pdf->SetFont('Arial','',12);
	$pdf->Cell(65,7, $odc.'/'.$req,0,0, 'L');
	$pdf->Cell(125,7,utf8_decode($instututo),0,1, 'L');
	$pdf->SetFont('Arial','B',12);
	$pdf->Cell(0,7,utf8_decode('Dirección de entrega:'),0,1, 'L');
	$pdf->SetFont('Arial','',12);
	$pdf->Cell(0,7,utf8_decode($direccion),0,1, 'L');
	$pdf->ln(10);
	$pdf->SetX(-200);
	$pdf->SetFont('Arial','B',10);
	$pdf->SetFillColor(0,230,230);
	$pdf->Cell(35,7, utf8_decode('Código'),1,0, 'C',true);
	$pdf->Cell(60,7, utf8_decode('Descripción del producto'),1,0, 'C',true);
	$pdf->Cell(35,7, utf8_decode('Marca'),1,0, 'C',true);
	$pdf->Cell(30,7, utf8_decode('Presentación'),1,0, 'C',true);
	$pdf->Cell(30,7, 'Cant. entregada',1,0, 'C',true);
	$pdf->ln(7);
	$pdf->SetFont('Arial','',10);
	$pdf->SetWidths(array(35,60,35,30,30));
	$queryproduct=mysqli_query($conexion, "SELECT a.id_renglon, a.cantidad, b.id_producto, c.codigo, c.descripcion, c.marca, c.unidad_medida FROM detalle_entrega a, productos_participacion b, productos c WHERE a.id_entrega='$id_entrega' AND b.id_participacion=a.id_renglon AND  c.id_producto=b.id_producto") or die (mysqli_error($conexion));
   
	while ($arrayproducts=mysqli_fetch_array($queryproduct)) {
		$pdf->Row(array(utf8_decode($arrayproducts['codigo']),utf8_decode($arrayproducts['descripcion']),utf8_decode($arrayproducts['marca']),utf8_decode($arrayproducts['unidad_medida']),$arrayproducts['cantidad']));
	}

	$pdf->Output('', 'prorroga-acto.pdf','');
?>
