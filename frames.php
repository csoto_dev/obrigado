<?php
	session_start();
	if(!isset($_SESSION['usuario'])){
		echo "<script>alert('Error 03: No puede acceder sin iniciar sesion.'); window.location='index.php';</script>";
	}
	include ("libs/conexion.php");
	include ("libs/privilegios.php");	
	$namepage=array();

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>.:Obrigado Gestion Cotrol System:.</title>


<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/bootstrap-table.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">

<!--Icons-->
<script src="js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	<?php include('navbar.html');?>
		
	<?php 
		if(isset($_GET['p'])){
			$p=$_GET['p']; 
			include($p.'.html');
		}else{			
			include('inicio.html');
		}


	?>
	<?php include('sidebar.html');?>
		

	  

</body>

</html>
