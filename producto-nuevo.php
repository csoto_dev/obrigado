<?php
	include('libs/conexion.php');
	include('libs/renglones.php');
	session_start();
	if(!isset($_SESSION['usuario'])){
		echo "<script>alert('Error 03: No puede acceder sin iniciar sesion.'); window.location='index.html';</script>";
	}

	$id_acto=$_POST['id_acto'];
	$id_renglon=$_POST['id_renglon'];
	$url=$_POST['url'];    
	$busqueda=$_POST['busqueda'];

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>.:Obrigado Licitaciones:.</title>


<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/bootstrap-table.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">

<!--Icons-->
<script src="js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body style="padding: 0;">
			<div class="row" style="margin:0;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">Registro de producto</div>
						<div class="panel-body">
							<form role="form" name="formulario" action="" method="POST">
							<div class="col-md-6">
									<div class="form-group" id="vcode">
										<label>Modelo del producto</label>
										<input class="form-control" id="code" name="codigo"  placeholder="Codigo del producto">
									</div>
									<div class="form-group" id="vdesc">
										<label>Descripci&oacute;n del producto</label>
										<textarea class="form-control" id="desc" name="descripcion"  placeholder="Descrici&oacute;n del producto" ></textarea>
									</div>
									<div class="form-group" id="vmarca">
										<label>Marca del producto</label>
										<input class="form-control" id="marca" name="marca" placeholder="Marca del producto" value='' >
									</div>				
									
									<div class="form-group" id="vum">
										<label>Unidad de medida</label>
										<input class="form-control" name="unid_med" id="um" placeholder="Unidad de medida" >
									</div>

									<div class="form-group"  id="vtipo">
									<label>Tipo de producto</label>
									<select name="tipo" class="form-control" id="tipo">
										<option>- - -</option>
										<?php
											$qtp=mysqli_query($conexion, "SELECT * FROM tipo_producto");
											while($atp=mysqli_fetch_array($qtp)){
												echo '<option value="'.$atp['id_tipop'].'">'.$atp['nombre'].'</option>';
											}
										?>
									</select>
									
		
									
								</div>
								<input type="hidden" name="id_acto" value="<?php echo $id_acto;?>">
								<input type="hidden" name="id_renglon" value="<?php echo $id_renglon;?>">
								<input type="hidden" name="url" value="<?php echo $url;?>">
								<input type="hidden" name="busqueda" value="<?php echo $busqueda;?>">
									<div class="form-group" >	
										<button type="button" onclick="validar()" class="btn btn-primary">Registrar</button>
									</div>
							</form>
						</div>
					</div>
				</div><!-- /.col-->
			</div><!-- /.row -->
	  

	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/common.js"></script>
	<script src="js/datepicker.js"></script>
	<script src="js/datepicker.es-ES.js"></script>
	<script src="js/main.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/bootstrap-table.js"></script>		   
	<script type="text/javascript">
		
		var listado_renglones= <?php echo $listado_renglones;?>;
		$(function () {
		    $('#listado-renglones').bootstrapTable({
		        data: listado_renglones
		    });
		});
	</script>	
	<script>
		$(document).ready( function(){
  			$("input[type=checkbox]").each(function(){
				var valor = $(this).val();
				var seleccionado = <?php echo $id_renglon; ?>;
				if(valor == seleccionado){
					$(this).prop("checked", true);
				}
		    	$('#id_renglon2').val(seleccionado);
		    	$('.slcall').prop("checked", false);
		    	$('.slcall').prop("disabled", true);
		    	$('.slcall').prop("style", 'display:none');
			}); 
		});

		$(document).on('change', "input[type='checkbox']" , function(){
  
		  // in the handler, 'this' refers to the box clicked on
 		var $box = $(this);
		 if ($box.is(":checked")) {
		    // the name of the box is retrieved using the .attr() method
		    // as it is assumed and expected to be immutable
		    var group = "input:checkbox[name='" + $box.attr("name") + "']";
		    // the checked state of the group/box on the other hand will change
		    // and the current value is retrieved using .prop() method
		    $(group).prop("checked", false);
		    $box.prop("checked", true);
		    $('.slcall').prop("checked", false);
		    $('.slcall').prop("disabled", true);
		    $('#id_renglon').val($box.val());

		  } else {
		    $box.prop("checked", false);
		    $('#id_renglon').val('');
		  }
		});
	</script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
	<script>

		function validar() {
			var code=document.getElementById('code').value;			
			var desc=document.getElementById('desc').value;	
			var marca=document.getElementById('marca').value;
			var um=document.getElementById('um').value;		
			var error=0;		


			document.getElementById("vcode").className -= " has-error";
			document.getElementById("vdesc").className -= " has-error";
			document.getElementById("vum").className -= " has-error";
			document.getElementById("vmarca").className -= " has-error";


			if(code.length==0){
				alert('El campo "Codigo de producto" no puede ir vacio.');
				document.getElementById("vcode").className += " has-error";
				error++;				
			}

			if(desc.length==0){
				alert('El campo "Descripcion" no puede ir vacio.');
				document.getElementById("vdesc").className += " has-error";
				error++;
				
			}

			if(marca.length==0){
				alert('El campo "Marca" no puede ir vacio.');
				document.getElementById("vmarca").className += " has-error";
				error++;
				
			}

			if(um.length==0){
				alert('El campo "Unidad de medida" no puede ir vacio.');
				document.getElementById("vum").className += " has-error";
				error++;
				
			}

			if(error==0){
				document.formulario.action= "reg_producto.php";
				document.formulario.submit();
			}
		}

	    // Controlamos que si pulsamos escape se cierre el div

	    $(document).keyup(function(event){
	        if(event.which==27){
				window.close();
	        }
	    });


	</script>	
	
</body>

</html>
