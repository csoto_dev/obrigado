<?php
	include('libs/conexion.php');
	session_start();
	if(!isset($_SESSION['usuario'])){
		echo "<script>alert('Error 03: No puede acceder sin iniciar sesion.'); window.location='index.html';</script>";
	}

		$queryreng=mysqli_query($conexion, "SELECT * FROM renglones_acto WHERE id_renglon='".$_GET["id"]."'") or die (mysqli_error($conexion));
		$arrayreng=mysqli_fetch_array($queryreng);
		$cantidad=$arrayreng['cantidad'];

	if(isset($_POST['id_producto'])){
		$id_producto=$_POST['id_producto'];
		$queryproduct=mysqli_query($conexion, "SELECT * FROM productos WHERE id_producto='$id_producto'") or die (mysqli_error($conexion));
		$arrayproduct=mysqli_fetch_array($queryproduct);
		$codigo=$arrayproduct['codigo'];
		$descripcion=$arrayproduct['descripcion'];
		$marca=$arrayproduct['marca'];
		$unidad_medida=$arrayproduct['unidad_medida'];
	}else{
		$id_producto='';
		$codigo='';
		$descripcion='';
		$marca='';
		$unidad_medida='';

	}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>.:Obrigado Licitaciones:.</title>


<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/bootstrap-table.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">

<!--Icons-->
<script src="js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body style="padding: 0;">

			<div class="row" style="margin:0;">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Registro de propuestas</div>
					<div class="panel-body">
						<form role="form" name="formbusqueda" action="busqueda.php" method="POST">
							<div class="col-md-6">								
								<div class="form-group" id="vbusqueda">
									<label style="display:block;">Buscar producto:</label>
									<input class="form-control" name="busqueda" id="busqueda" placeholder="Buscar producto" style="width:70%; display:inline-block;">

									<input type='hidden' name="id_acto" value='<?php echo $_GET["id"];?>'>
									<input type='hidden' name="url" value='registro-detalle-venta.php'>

									<button type="button" onclick="buscar_producto()" class="btn btn-primary">Buscar</button>
								</div>
								
							</div>
						</form>
						<form role="form" name="formulario" action="" method="POST">
						<div class="col-md-6">
								<div class="form-group" id="vcode">
									<label>C&oacute;digo del producto</label>
									<input class="form-control" id="code" placeholder="Unidad de medida" value='<?php echo $codigo;?>' readonly>
								</div>
								<div class="form-group" id="vdesc">
									<label>Descripci&oacute;n del producto</label>
									<textarea class="form-control" id="" placeholder="Descrici&oacute;n del producto" readonly><?php echo $descripcion;?></textarea>
								</div>
								<div class="form-group" id="vmarca">
									<label>Marca del producto</label>
									<input class="form-control" id="" placeholder="Marca del producto" value='<?php echo $marca;?>' readonly>
								</div>				
								
								<div class="form-group" id="vum">
									<label>Unidad de medida</label>
									<input class="form-control" name="unid_medida" id="um" placeholder="Unidad de medida" value='<?php echo $unidad_medida;?>' readonly>
								</div>
								<div class="form-group" id="vpreciou">
									<label>Precio unit.</label>
									<input class="form-control" name="preciou" type="number" value='0' step="0.01" min="0" id="preciou" placeholder="Clasificaci&oacute;n" onchange="cambiar_total()">
									<input type='hidden' name="id_producto" value='<?php echo $id_producto;?>'>
								</div>
								<div class="form-group" id="vpreciou">
									<label>Cantidad</label>
									<input class="form-control" type="number" name='cantidad' min="0" value="0" id="cantidad"  onchange="cambiar_total()">
								</div>
								<div class="form-group" id="vpreciot">
									<label>Precio total.</label>
									<input class="form-control" name="preciot" id="preciot" readonly="">
									<input type='hidden' name="id_venta" value='<?php echo $_GET["id"];?>'>
								</div>
								
								
							</div>
								<div class="form-group" >	
									<button type="button" onclick="validar()" class="btn btn-primary">Registrar</button>
								</div>
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
	  

  <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/common.js"></script>
  <script src="js/datepicker.js"></script>
  <script src="js/datepicker.es-ES.js"></script>
  <script src="js/main.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/bootstrap-table.js"></script>	  
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>
	<script type="text/javascript">
		 // Controlamos que si pulsamos escape se cierre el div

	    $(document).keyup(function(event){
	        if(event.which==27){
				window.close();
	        }
	    });
	    

	    function buscar_producto() {
	    	var busqueda=document.getElementById('busqueda').value;
			var error=0;
			document.getElementById("vbusqueda").className -= " has-error";
			if(busqueda.length==0){
				alert('El campo de busqueda no puede enviarse vacio.');
				document.getElementById("vbusqueda").className += " has-error";
				error++;
				
			}
			if(error==0){
				document.formbusqueda.action= "busqueda.php";
				document.formbusqueda.submit();
			}
	    }
	</script>	
	<script>
		function cambiar_total() {
			var cantidad = document.getElementById('cantidad').value;
			var preciou=document.getElementById('preciou').value; 
			var preciotot = parseFloat(preciou).toFixed(2) * parseFloat(cantidad).toFixed(2);


			document.getElementById('preciot').value=parseFloat(preciotot).toFixed(2);

		}
		function validar() {
			var codigo=document.getElementById('code').value;			
			var um=document.getElementById('um').value;	
			var preciou=document.getElementById('preciou').value;
			var preciot=document.getElementById('preciot').value;			
			var error=0;		

			document.getElementById("vcode").className -= " has-error";
			document.getElementById("vdesc").className -= " has-error";
			document.getElementById("vmarca").className -= " has-error";
			document.getElementById("vum").className -= " has-error";
			document.getElementById("vpreciou").className -= " has-error";
			document.getElementById("vpreciot").className -= " has-error";


			if(codigo.length==0){
				alert('Debe seleccionar un producto.');
				document.getElementById("vcode").className += " has-error";
				document.getElementById("vdesc").className += " has-error";
				document.getElementById("vmarca").className += " has-error";
				error++;
				
			}

			if(um.length==0){
				alert('El campo "Unidad de medida" no puede ir vacio.');
				document.getElementById("vum").className += " has-error";
				error++;
				
			}

			if(preciou.length==0){
				alert('El campo "Unidad de medida" no puede ir vacio.');
				document.getElementById("vpreciou").className += " has-error";
				error++;
				
			}

			if(preciot.length==0){
				alert('El campo "Unidad de medida" no puede ir vacio.');
				document.getElementById("vpreciot").className += " has-error";
				error++;
				
			}

			if(error==0){
				document.formulario.action= "reg_detalle_venta.php";
				document.formulario.submit();
			}
		}


	</script>	
	
</body>

</html>
