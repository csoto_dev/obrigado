<?php
	session_start();
	if(!isset($_SESSION['usuario'])){
		echo "<script>alert('Error 03: No puede acceder sin iniciar sesion.'); window.location='index.html';</script>";
	}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>.:Obrigado Licitaciones:.</title>


<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/bootstrap-table.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">

<!--Icons-->
<script src="js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body style="padding: 0;">

			<div class="row" style="margin:0;">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Registro de renglones</div>
					<div class="panel-body">
							<form role="form" name="formulario" action="" method="POST">	
						<div class="col-md-6">
								<div class="form-group" id="vnro">
									<label>Nro del renglon</label>
									<input class="form-control" min="0"  value="0" type="number" name="nro_renglon" id="nro" placeholder="Nro. del renglon">
								</div>
								
								<div class="form-group" id="vcodigo">
									<label>C&oacute;digo</label>
									<input class="form-control" name="codigo" id="codigo" placeholder="C&oacute;digo del producto">
								</div>
								<div class="form-group" id="vdescripcion">
									<label>Descripci&oacute;n</label>
									<textarea class="form-control" rows="4" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n"></textarea>
								</div>
								
							</div>
						<div class="col-md-6">	
								
								<div class="form-group" id="vum">
									<label>Unidad de medida</label>
									<input class="form-control" name="unid_medida" id="um" placeholder="Unidad de medida">
								</div>
								<div class="form-group" id="vcantidad">
									<label>Cantidad</label>
									<input class="form-control" name="cantidad" min="0" step="0.01" value="0" type="number" id="cantidad" placeholder="Cantidad">
								</div>
								<div class="form-group" id="vclasificacion">
									<label>Clasificaci&oacute;n</label>
									<input class="form-control" name="clasificacion" id="clasificacion" placeholder="Clasificaci&oacute;n">
									<input type='hidden' name="id" value='<?php echo $_GET["id"];?>'>
								</div>
								
								
							</div>
								<div class="form-group" >	
									<button type="button" onclick="validar()" class="btn btn-primary">Registrar</button>
								</div>
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
	  

  <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/common.js"></script>
  <script src="js/datepicker.js"></script>
  <script src="js/datepicker.es-ES.js"></script>
  <script src="js/main.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/bootstrap-table.js"></script>	  
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
	<script>
		function validar() {
			var codigo=document.getElementById('codigo').value;			
			var descripcion=document.getElementById('descripcion').value;
			var um=document.getElementById('um').value;			
			var cantidad=document.getElementById('cantidad').value;
			var clasificacion=document.getElementById('clasificacion').value;	
			var nro=document.getElementById('nro').value;	
			var error=0;

			document.getElementById("vcodigo").className -= " has-error";
			document.getElementById("vdescripcion").className -= " has-error";
			document.getElementById("vum").className -= " has-error";
			document.getElementById("vcantidad").className -= " has-error";
			document.getElementById("vclasificacion").className -= " has-error";
			document.getElementById("vnro").className -= " has-error";


			if(codigo.length==0){
				alert('El campo "Codigo" no puede ir vacio.');
				document.getElementById("vcodigo").className += " has-error";
				error++;
				
			}

			if(descripcion.length==0){
				alert('El campo "Descripcion" no puede ir vacio.');
				document.getElementById("vdescripcion").className += " has-error";
				error++;
				
			}
			if(um.length==0){
				alert('El campo "Unidad de medida" no puede ir vacio.');
				document.getElementById("vum").className += " has-error";
				error++;
				
			}

			if(cantidad.length==0){
				alert('El campo "Cantidad" no puede ir vacio.');
				document.getElementById("vcantidad").className += " has-error";
				error++;				
			}

			if(cantidad<=0){
				alert('El campo "Cantidad" debe ser mayor a 0.');
				document.getElementById("vcantidad").className += " has-error";
				error++;				
			}

			if(nro.length==0){
				alert('El campo "Nro. de renglon" no puede ir vacio.');
				document.getElementById("vnro").className += " has-error";
				error++;				
			}

			if(nro<=0){
				alert('El campo "Nro. de renglon" debe ser mayor a 0.');
				document.getElementById("vnro").className += " has-error";
				error++;				
			}

			if(clasificacion.length==0){
				alert('El campo "Clasificacion" no puede ir vacio.');
				document.getElementById("vclasificacion").className += " has-error";
				error++;				
			}


			if(error==0){
				document.formulario.action= "reg_renglon.php";
				document.formulario.submit();
			}
		}

	    // Controlamos que si pulsamos escape se cierre el div

	    $(document).keyup(function(event){
	        if(event.which==27){
				window.close();
	        }
	    });


	</script>	
	
</body>

</html>
