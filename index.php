<?php 
	session_start();
	if(isset($_SESSION["usuario"])){		
			echo "<script>window.location='frames.php?p=inicio';</script>";
	}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>.:Obrigado Gestion Cotrol System:.</title>

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	
	<div class="panel-heading" align="center">OBRIGADO MEDICAL GROUP - CONTROL GESTION SYSTEM </div>
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Inicio de sesi&oacute;n</div>
				<div class="panel-body">
					<form role="form" action="login.php" name="formlogin" method="post">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="Usuario" name="usuario" type="text" autofocus="">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Contrase&ntilde;a" name="password" type="password" value="">
							</div>
							<a onclick="enviar()" class="btn btn-primary">Login</a>
						</fieldset>
					</form>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->	
	
		

	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
	<script>
		function enviar() {
			document.formlogin.submit();
		}
		$(document).keyup(function(event){
            if(event.which===13 ){
                enviar();
            }
        });
	</script>	
</body>

</html>
