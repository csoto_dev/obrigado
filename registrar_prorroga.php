<?php
    include("libs/conexion.php");
    require_once "libs/feriados.php";

    	//declaracion de variables
		$id_acto=$_POST['id_acto'];
		$cargo=$_POST['cargo'];
		$nombre=$_POST['nombre'];
		$cantidad_dias=$_POST['cantidad_dias'];
		$x=0;
		$tipo_dias=$_POST['tipo_dias'];
		$motivo=$_POST['motivo'];
		$fecha_emision=date('Y-m-d');

		//query para saber cuantas prorrogas lleva el acto
		$queryact=mysqli_query($conexion, "SELECT nro_prorrogas, fecha_max_entrega FROM actos WHERE id_acto='$id_acto'");
		$arrayact=mysqli_fetch_array($queryact);
		$fecha_entrega=$arrayact['fecha_max_entrega'];
		// calcular fecha de vencimiento

		if($tipo_dias=='HABILES'){

			
			for ($i=0; $i < $cantidad_dias; $i++) { 
				

				$x++;
				$dia=date('l', strtotime('+'.$x.' day', strtotime($fecha_entrega)));
				switch ($dia) {
					case 'Saturday':
						$i--;
						break;

					case 'Sunday':
						$i--;
						break;
					
					default:
						for ($y=0; $y < 11; $y++) { 
							if($dia == $feriadosano[$i]){
								$i--;
							}
						}
						break;
				}

			}

			$vencimiento=date('Y-m-d', strtotime('+'.$x.' day', strtotime($fecha_entrega)));
			
		}else{
			$vencimiento=date('Y-m-d', strtotime('+'.$cantidad_dias.' day', strtotime($fecha_entrega)));
		}

		//registrando datos generales prorroga
		mysqli_query($conexion, "INSERT INTO prorrogas SET id_acto='$id_acto', cantidad_dias='$cantidad_dias', tipo_dias='$tipo_dias', fecha_emision='$fecha_emision', fecha_vencimiento='".$vencimiento."', dirigidoa='".$nombre."', cargoa='".$cargo."', motivo='$motivo',estado='1'") or die (mysqli_error($conexion));
		$id_prorroga=mysqli_insert_id($conexion);


		//ciclo de registro de mercancia en la prorroga
		$nprod=count($_POST['id_productos']);
		for ($w=0; $w < $nprod; $w++) { 
			$id_productos=$_POST['id_productos'][$w];
			mysqli_query($conexion, "INSERT INTO  productos_prorroga SET id_prorroga='$id_prorroga', id_propuesta='$id_productos'") or die (mysqli_error($conexion));
		}

		//actualizamos el numero de prorrogas emitidas en el registro del acto
		$newnumber=$arrayact['nro_prorrogas']+1;
		mysqli_query($conexion, "UPDATE actos SET nro_prorrogas='$newnumber' WHERE id_acto='$id_acto'");

		//muestra el pdf
		echo"
			<script type='text/javascript'>
				window.open('mostrar-prorroga.php?id=".$id_prorroga."','popup','toolbars=no, directories=no,menubar=no,status=no,resiz eable=0,width=800,height=900');
				window.location='frames.php?p=prorrogas';
			</script>";
	
?>
