	<?php
		include("libs/conexion.php");
	?>	
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md-6">
							<form role="form" name="formulario" action="" method="POST">	
							<div class="form-group" id="vinst">
									<label>Instituci&oacute;n</label>
									<select class="form-control" name="id_inst" id="inst">
										<option value="">- - -</option>
										<?php
											$queryinst=mysqli_query($conexion, "SELECT * FROM instituciones WHERE tipo='Publica'");
											while ($arrayinst=mysqli_fetch_array($queryinst)){
												if($arrayinst['id_inst']==$id_inst){
													echo "<option value='".$arrayinst['id_inst']."' selected>".$arrayinst['nombre']."</option>";
												}else{
													echo "<option value='".$arrayinst['id_inst']."'>".$arrayinst['nombre']."</option>";
												}

											}
										?>
									</select>
								</div>
								<div class="form-group" id="vfecha">
									<label>Fecha</label>
									<input class="form-control" type="date" name="fecha" id="fecha" value="<?php echo $fecha;?>">
								</div>
								<div class="form-group" id="vacto">
									<label>Nro. Acto</label>
									<input class="form-control" name="n_acto" id="acto" placeholder="Nro. de acto" value="<?php echo $n_acto;?>">
								</div>
								<div class="form-group" id="vreq">
									<label>Nro. Requisici&oacute;n</label>
									<input class="form-control" name="n_req" id="req" placeholder="Nro. de requisici&oacute;n" value="<?php echo $n_req;?>">
								</div>		
								<div class="form-group" id="vmonto">
									<label>Monto del acto</label>
									<input class="form-control" type="number" name="monto_act" id="monto" min="0" step="0.01" placeholder="0,00" value="<?php echo $monto_act;?>">
								</div>
								<div class="form-group" id="vapertura">
									<label>Fecha de apertura</label>
									<input class="form-control" type="date" name="fecha_apertura" id="apertura" value="<?php echo $fecha_apertura;?>">
								</div>
								<div class="form-group" id="vdesicion">
									<label>Fecha de desici&oacute;n</label>
									<input class="form-control" type="date" name="fecha_desicion" id="desicion" value="<?php echo $fecha_desicion;?>">
								</div>
								<div class="form-group" id="vODC">
									<label>Fecha de orden de compra</label>
									<input class="form-control" type="date" name="fecha_ODC" id="ODC" value="<?php echo $fecha_ODC;?>">
								</div>
								<div class="form-group" id="vODP">
									<label>Fecha de orden de pago</label>
									<input class="form-control" type="date" name="fecha_ODP" id="ODP" value="<?php echo $fecha_ODP;?>">
								</div>
								<div class="form-group" id="vpago">
									<label>Fecha de pago</label>
									<input class="form-control" type="date" name="fecha_pago" id="pago" value="<?php echo $fecha_pago;?>">
									<input type="hidden" name="id" value="<?php echo $id_acto;?>">
								</div>
								<div class="form-group" id="vstatus">
									<label>Estado</label>
									<select class="form-control" name="id_status" id="status">
										<option value="">- - -</option>
										<?php
											$querystatus=mysqli_query($conexion, "SELECT * FROM estado_licitaciones");
											while ($arraystatus=mysqli_fetch_array($querystatus)){
												if($id_status==$arraystatus['id_status']){
													echo "<option value='".$arraystatus['id_status']."' selected>".$arraystatus['nombre']."</option>";
												}else{
													echo "<option value='".$arraystatus['id_status']."'>".$arraystatus['nombre']."</option>";
												}

											}
										?>
									</select>
								</div>
								<div class="form-group" id="vpo">
									<label>&iquest;Participa obrigado?</label>
									<select class="form-control" name="po" id="po">
										<option value="">- - -</option>
										<option value="Si" <?php
											if($po=='Si'){
												echo "selected";
											}
										?>>Si</option>
										<option value="No"<?php
											if($po=='No'){
												echo "selected";
											}
										?>>No</option>
									</select>
								</div>
								<div class="form-group" >
									<button type="button" onclick="validar()" class="btn btn-primary">Registrar</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		
	</div><!--/.main-->

	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
	<script>
		function validar() {
			var inst=document.getElementById('inst').value;			
			var centro=document.getElementById('centro').value;
			var fecha=document.getElementById('fecha').value;			
			var acto=document.getElementById('acto').value;
			var req=document.getElementById('req').value;			
			var monto=document.getElementById('monto').value;
			var apertura=document.getElementById('apertura').value;		
			var po=document.getElementById('po').value;		

			var error=0;

			document.getElementById("vinst").className -= " has-error";
			document.getElementById("vcentro").className -= " has-error";
			document.getElementById("vfecha").className -= " has-error";
			document.getElementById("vacto").className -= " has-error";
			document.getElementById("vreq").className -= " has-error";
			document.getElementById("vmonto").className -= " has-error";
			document.getElementById("vapertura").className -= " has-error";
			document.getElementById("vpo").className -= " has-error";


			if(inst.length==0){
				alert("El campo institucion no puede ir vacio.");
				document.getElementById("vinst").className += " has-error";
				error++;
				
			}

			if(centro.length==0){
				alert("El campo centro no puede ir vacio.");
				document.getElementById("vcentro").className += " has-error";
				error++;
				
			}

			if(fecha.length==0){
				alert("El campo fecha no puede ir vacio.");
				document.getElementById("vfecha").className += " has-error";
				error++;				
			}

			if(acto.length==0){
				alert("El campo acto no puede ir vacio.");
				document.getElementById("vacto").className += " has-error";
				error++;				
			}

			if(req.length==0){
				alert("El campo requisicion no puede ir vacio.");
				document.getElementById("vreq").className += " has-error";
				error++;				
			}

			if(monto.length==0){
				alert("El campo monto del acto no puede ir vacio.");
				document.getElementById("vmonto").className += " has-error";
				error++;				
			}

			if(monto==0){
				alert("El monto no puede ser 0.");
				document.getElementById("vmonto").className += " has-error";
				error++;				
			}

			if(apertura.length==0){
				alert("El campo fecha de apertura no puede ir vacio.");
				document.getElementById("vapertura").className += " has-error";
				error++;				
			}

			if(po.length==0){
				alert("Debe seleccionar una opcion el en campo participa obrigado.");
				document.getElementById("vpo").className += " has-error";
				error++;				
			}

			if(error==0){
				document.formulario.action= "actualizar_licitacion.php";
				document.formulario.submit();
			}
		}
		
		function send2() {
			document.formulario.action= "<?php echo $_SERVER['REQUEST_URI'] ?>";
			document.formulario.submit();
		}

	</script>	